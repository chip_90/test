-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mar 18, 2020 alle 12:19
-- Versione del server: 10.4.11-MariaDB
-- Versione PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `highfly`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `airplane_service`
--

CREATE TABLE `airplane_service` (
  `service_id` int(11) NOT NULL,
  `seats_first_class` int(11) DEFAULT NULL,
  `seats_second_class` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `airplane_service`
--

INSERT INTO `airplane_service` (`service_id`, `seats_first_class`, `seats_second_class`) VALUES
(5, 50, 100),
(4, NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `customer_id` int(11) NOT NULL,
  `nominative` varchar(90) NOT NULL,
  `n_partecipants` int(11) NOT NULL,
  `state` varchar(45) NOT NULL,
  `version` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `booking`
--

INSERT INTO `booking` (`id`, `code`, `date`, `customer_id`, `nominative`, `n_partecipants`, `state`, `version`) VALUES
(1, 'c30320201500', '2020-03-14 15:09:51', 1, 'Gabriele Bilello', 0, 'WIP', 0),
(2, 'c140320201609', '2020-03-14 15:09:51', 3, 'Alessandro Calvani', 0, 'WIP', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `booking_detail`
--

CREATE TABLE `booking_detail` (
  `id` int(11) NOT NULL,
  `booking_code` varchar(45) NOT NULL,
  `departure_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `departure_place` varchar(45) NOT NULL,
  `destination_place` varchar(45) NOT NULL,
  `service_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `booking_detail`
--

INSERT INTO `booking_detail` (`id`, `booking_code`, `departure_date`, `departure_place`, `destination_place`, `service_id`, `version`) VALUES
(1, 'c30320201500', '2020-03-18 09:08:54', 'Palermo', 'Catania', 2, 0),
(2, 'c140320201609', '2020-03-18 09:08:58', 'Carrara', 'Milano', 4, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `car_service`
--

CREATE TABLE `car_service` (
  `service_id` int(11) NOT NULL,
  `supply` varchar(45) NOT NULL,
  `vehicle_quantity` int(11) NOT NULL DEFAULT 0,
  `manufacturer` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `car_service`
--

INSERT INTO `car_service` (`service_id`, `supply`, `vehicle_quantity`, `manufacturer`) VALUES
(2, 'Benzina', 5, 'Mercedes'),
(3, 'Gasolio', 2, 'Mercedes');

-- --------------------------------------------------------

--
-- Struttura della tabella `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `business_name` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `fiscal_code` varchar(16) DEFAULT NULL,
  `piva` varchar(15) DEFAULT NULL,
  `type` varchar(45) NOT NULL,
  `is_active` tinyint(4) DEFAULT 1,
  `version` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `customer`
--

INSERT INTO `customer` (`id`, `business_name`, `name`, `lastname`, `fiscal_code`, `piva`, `type`, `is_active`, `version`) VALUES
(1, NULL, 'Gabriele', 'Bilello', 'BLLGRL95T14G273L', NULL, 'persona fisica', 1, 0),
(2, NULL, 'Alessandro', 'Calvani', 'CLVLSN90R01F023C', NULL, 'persona fisica', 1, 0),
(3, 'Golden Tour', NULL, NULL, NULL, '01523665986', 'Company', 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `version` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `role`
--

INSERT INTO `role` (`id`, `code`, `description`, `version`) VALUES
(1, 'a001', 'admin', 0),
(2, 'g001', 'guest', 0),
(3, 'o001', 'operator', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `service`
--

CREATE TABLE `service` (
  `service_id` int(11) NOT NULL,
  `seats` int(11) DEFAULT NULL,
  `model` varchar(45) NOT NULL,
  `vehicle` varchar(45) NOT NULL,
  `version` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `service`
--

INSERT INTO `service` (`service_id`, `seats`, `model`, `vehicle`, `version`) VALUES
(1, 200, 'locomotiva elettrica', 'Train', 0),
(2, 2, 'Smart', 'Car', 2),
(3, 4, 'CLK', 'Car', 0),
(4, 1, 'Caccia', 'Airplane', 0),
(5, 150, 'Boeng 737', 'Airplane', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `train_service`
--

CREATE TABLE `train_service` (
  `service_id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `wagons_number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `train_service`
--

INSERT INTO `train_service` (`service_id`, `type`, `wagons_number`) VALUES
(1, 'Regionale', 6);

-- --------------------------------------------------------

--
-- Struttura della tabella `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role_code` varchar(45) NOT NULL,
  `version` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `user`
--

INSERT INTO `user` (`id`, `name`, `surname`, `email`, `username`, `password`, `role_code`, `version`) VALUES
(1, 'admin', 'admin', 'admin@admin.it', 'admin', 'admin', 'a001', 0),
(2, 'guest', 'guest', 'guest@guest.it', 'guest', 'guest', 'g001', 0),
(3, 'pippo', 'pippo', 'pippo@admin.it', 'pippo', 'pippo', 'g001', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `airplane_service`
--
ALTER TABLE `airplane_service`
  ADD KEY `service_id` (`service_id`);

--
-- Indici per le tabelle `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indici per le tabelle `booking_detail`
--
ALTER TABLE `booking_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_code` (`booking_code`),
  ADD KEY `service_id` (`service_id`);

--
-- Indici per le tabelle `car_service`
--
ALTER TABLE `car_service`
  ADD KEY `service_id` (`service_id`);

--
-- Indici per le tabelle `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fiscal_code` (`fiscal_code`),
  ADD UNIQUE KEY `piva` (`piva`);

--
-- Indici per le tabelle `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indici per le tabelle `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indici per le tabelle `train_service`
--
ALTER TABLE `train_service`
  ADD KEY `service_id` (`service_id`);

--
-- Indici per le tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_code` (`role_code`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `booking_detail`
--
ALTER TABLE `booking_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `service`
--
ALTER TABLE `service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `airplane_service`
--
ALTER TABLE `airplane_service`
  ADD CONSTRAINT `airplane_service_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`);

--
-- Limiti per la tabella `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

--
-- Limiti per la tabella `booking_detail`
--
ALTER TABLE `booking_detail`
  ADD CONSTRAINT `booking_detail_ibfk_1` FOREIGN KEY (`booking_code`) REFERENCES `booking` (`code`),
  ADD CONSTRAINT `booking_detail_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`);

--
-- Limiti per la tabella `car_service`
--
ALTER TABLE `car_service`
  ADD CONSTRAINT `car_service_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`);

--
-- Limiti per la tabella `train_service`
--
ALTER TABLE `train_service`
  ADD CONSTRAINT `train_service_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`);

--
-- Limiti per la tabella `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_code`) REFERENCES `role` (`code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
