package com.lipari.highfly.utils;

import java.util.ArrayList;
import java.util.List;

import com.lipari.highfly.dto.BookingDetailDto;
import com.lipari.highfly.dto.BookingDto;
import com.lipari.highfly.entities.BookingDetail;

public class BookingDetailUtils {

	public static List<BookingDetailDto> fromEntityToDto(List<BookingDetail> booking) {
		
		List<BookingDetailDto> result = new ArrayList<BookingDetailDto>();
		
		booking.stream().forEach(
				b -> result.add(new BookingDetailDto
						(
						b.getId(), 
						b.getDepartureDate(), 
						b.getDeparturePlace(), 
						b.getDestinationPlace(), 
						ServiceUtils.fromEntityToDto(b.getService())
						)));

		return result;
	}
	
	public static BookingDetail fromDtoToEntity(BookingDetailDto bookingDetailDto) {

		BookingDetail bookingDetail = new BookingDetail();

		bookingDetail.setId(bookingDetailDto.getId());
		bookingDetail.setDepartureDate(bookingDetailDto.getDepartureDate());
		bookingDetail.setDeparturePlace(bookingDetailDto.getDeparturePlace());
		bookingDetail.setDestinationPlace(bookingDetailDto.getDestinationPlace());
		bookingDetail.setService(ServiceUtils.fromDtoToEntity(bookingDetailDto.getService()));
		
		return bookingDetail;
	}
	
	public static BookingDetail fromDtoToEntity(BookingDetailDto bookingDetailDto, BookingDto booking) {

		BookingDetail bookingDetail = new BookingDetail();

		bookingDetail.setId(bookingDetailDto.getId());
		bookingDetail.setDepartureDate(bookingDetailDto.getDepartureDate());
		bookingDetail.setDeparturePlace(bookingDetailDto.getDeparturePlace());
		bookingDetail.setDestinationPlace(bookingDetailDto.getDestinationPlace());
		bookingDetail.setService(ServiceUtils.fromDtoToEntity(bookingDetailDto.getService()));
		bookingDetail.setBooking(BookingUtils.fromDtoToEntity(booking));

		return bookingDetail;
	}

	public static void injectFromDtoToEntity(BookingDetail bookingDetail, BookingDetailDto bookingDetailDto) {
		
		bookingDetail.setId(bookingDetailDto.getId());
		bookingDetail.setDepartureDate(bookingDetailDto.getDepartureDate());
		bookingDetail.setDeparturePlace(bookingDetailDto.getDeparturePlace());
		bookingDetail.setDestinationPlace(bookingDetailDto.getDestinationPlace());
		bookingDetail.setService(ServiceUtils.fromDtoToEntity(bookingDetailDto.getService()));
	}
}