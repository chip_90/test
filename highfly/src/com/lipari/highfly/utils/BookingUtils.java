package com.lipari.highfly.utils;

import java.util.ArrayList;
import java.util.List;

import com.lipari.highfly.dto.BookingDetailDto;
import com.lipari.highfly.dto.BookingDto;
import com.lipari.highfly.entities.Booking;
import com.lipari.highfly.entities.BookingDetail;

public class BookingUtils {
	
	public static BookingDto fromEntityToDto(Booking booking) {
		
		BookingDto bDto = new BookingDto();		
		
		bDto.setId(booking.getId());
		bDto.setCode(booking.getCode());
		bDto.setDate(booking.getDate());
		bDto.setNominative(booking.getNominative());
		bDto.setnPartecipants(booking.getnPartecipants());
		bDto.setState(booking.getState());
		bDto.setCustomer(CustomerUtils.fromEntityToDto(booking.getCustomer()));
		
		bDto.setBookingDetails(BookingDetailUtils.fromEntityToDto(booking.getBookingDetails()));

		return bDto;
	}

	public static Booking fromDtoToEntity(BookingDto bookingDto) {

		Booking booking = new Booking();
				
		booking.setCode(bookingDto.getCode());
		booking.setId(bookingDto.getId());
		booking.setDate(bookingDto.getDate());
		booking.setNominative(bookingDto.getNominative());
		booking.setnPartecipants(bookingDto.getnPartecipants());
		booking.setState(bookingDto.getState());
		
		if (bookingDto.getCustomer() != null) {
			booking.setCustomer(CustomerUtils.fromDtoToEntity(bookingDto.getCustomer()));
		}
		
		return booking;
	}

	public static void injectFromDtoToEntity(Booking booking, BookingDto bookingDto) {
		booking.setId(bookingDto.getId());
		booking.setCode(bookingDto.getCode());
		booking.setDate(bookingDto.getDate());
		booking.setNominative(bookingDto.getNominative());
		booking.setnPartecipants(bookingDto.getnPartecipants());

		booking.setState(bookingDto.getState());
		
		if (bookingDto.getCustomer() != null) {
			booking.setCustomer(CustomerUtils.fromDtoToEntity(bookingDto.getCustomer()));
		}
		if (bookingDto.getBookingDetails() != null) {
			List<BookingDetail> bookingDetailList = new ArrayList<BookingDetail>();
			for (BookingDetailDto bookingDetailDto : bookingDto.getBookingDetails()) {
							
				bookingDetailList.add(BookingDetailUtils.fromDtoToEntity(bookingDetailDto));
			}
			booking.setBookingDetails(bookingDetailList);
		}	
	}
}