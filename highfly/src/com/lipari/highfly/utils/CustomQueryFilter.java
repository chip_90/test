/**
 * CustomQueryFilter.java
 *
 * robgion
 * www.2clever.it
 * 
 * 30 ott 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.utils;

/**
 * @author robgion
 *
 */
public class CustomQueryFilter {

	private String fieldName;
	private Object fieldValue;
	
	private String joinFieldName;

	public String getFieldName() {
		return fieldName;
	}
	
	public CustomQueryFilter() {
	}
	
	public CustomQueryFilter(String fieldName, Object fieldValue) {
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Object getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getJoinFieldName() {
		return joinFieldName;
	}

	public void setJoinFieldName(String joinFieldName) {
		this.joinFieldName = joinFieldName;
	}
	
}