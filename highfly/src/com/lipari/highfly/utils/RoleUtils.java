package com.lipari.highfly.utils;

import com.lipari.highfly.dto.RoleDto;
import com.lipari.highfly.entities.Role;

public class RoleUtils {

	public static RoleDto fromEntityToDto(Role r) {
		
		RoleDto rDto = new RoleDto();
		
		rDto.setId(r.getId());
		rDto.setCode(r.getCode());
		rDto.setDescription(r.getDescription());

		return rDto;
	}
	
	public static Role fromDtoToEntity(RoleDto rDto) {
		
		Role r = new Role();
		
		r.setId(rDto.getId());
		r.setCode(rDto.getCode());
		r.setDescription(rDto.getDescription());

		return r;
	}
	
	public static void injectFromDtoToEntity(Role r, RoleDto rDto) {
		
		r.setId(rDto.getId());
		r.setCode(rDto.getCode());
		r.setDescription(rDto.getDescription());

	}
}