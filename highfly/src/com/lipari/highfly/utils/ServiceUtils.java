package com.lipari.highfly.utils;

import com.lipari.highfly.dto.AirplaneServiceDto;
import com.lipari.highfly.dto.CarServiceDto;
import com.lipari.highfly.dto.ServiceDto;
import com.lipari.highfly.dto.TrainServiceDto;
import com.lipari.highfly.entities.AirplaneService;
import com.lipari.highfly.entities.CarService;
import com.lipari.highfly.entities.Service;
import com.lipari.highfly.entities.TrainService;

public class ServiceUtils {

	public static ServiceDto fromEntityToDto(Service s) {
		
		ServiceDto sDto = null;
		
		if(s.getVehicle().equals("Car")) {
			
			sDto = new CarServiceDto();
			
			CarService cS= (CarService) s;
			
			sDto = CarServiceUtils.fromEntityToDto(cS);
		}
		else if(s.getVehicle().equals("Train")) {
			
			sDto = new TrainServiceDto();
			
			TrainService tS= (TrainService) s;
			
			sDto = TrainServiceUtils.fromEntityToDto(tS);
		}
		
		else if(s.getVehicle().equals("Airplane")) {
			
			sDto = new AirplaneServiceDto();
			
			AirplaneService aS= (AirplaneService) s;
			
			sDto = AirplaneServiceUtils.fromEntityToDto(aS);
		}
		
		sDto.setServiceId(s.getServiceId());
		sDto.setModel(s.getModel());
		sDto.setSeats(s.getSeats());
		sDto.setVehicle(s.getVehicle());

		return sDto;
	}
	
	public static Service fromDtoToEntity(ServiceDto sD) {
		
		Service s = null;
		
		if(sD.getVehicle().equals("Car")) {
			
			s = new CarService();
			
			CarServiceDto cSDto= (CarServiceDto) sD;
			
			s = CarServiceUtils.fromDtoToEntity(cSDto);
		}
		else if(sD.getVehicle().equals("Train")) {
			
			s = new TrainService();
			
			TrainServiceDto tSDto= (TrainServiceDto) sD;
			
			s = TrainServiceUtils.fromDtoToEntity(tSDto);
		}
		
		else if(sD.getVehicle().equals("Airplane")) {
			
			s = new AirplaneService();
			
			AirplaneServiceDto aSDto= (AirplaneServiceDto) sD;
			
			s = AirplaneServiceUtils.fromDtoToEntity(aSDto);
		}
		
		s.setServiceId(sD.getServiceId());
		s.setModel(sD.getModel());
		s.setSeats(sD.getSeats());
		s.setVehicle(sD.getVehicle());

		return s;
	}
	
	public static void injectFromDtoToEntity(Service s, ServiceDto sD) {
		
		if(sD.getVehicle().equals("Car")) {
			
			s = new CarService();
			
			CarServiceDto cSDto= (CarServiceDto) sD;
			
			s = CarServiceUtils.fromDtoToEntity(cSDto);
		}
		else if(sD.getVehicle().equals("Train")) {
			
			s = new TrainService();
			
			TrainServiceDto tSDto= (TrainServiceDto) sD;
			
			s = TrainServiceUtils.fromDtoToEntity(tSDto);
		}
		
		else if(sD.getVehicle().equals("Airplane")) {
			
			s = new AirplaneService();
			
			AirplaneServiceDto aSDto= (AirplaneServiceDto) sD;
			
			s = AirplaneServiceUtils.fromDtoToEntity(aSDto);
		}
		
		s.setServiceId(sD.getServiceId());
		s.setModel(sD.getModel());
		s.setSeats(sD.getSeats());
		s.setVehicle(sD.getVehicle());
	}
}