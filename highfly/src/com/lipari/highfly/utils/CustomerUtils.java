package com.lipari.highfly.utils;

import com.lipari.highfly.dto.CustomerDto;
import com.lipari.highfly.entities.Customer;

public class CustomerUtils {

	public static CustomerDto fromEntityToDto(Customer customer) {

		CustomerDto customerDto = new CustomerDto();
		customerDto.setId(customer.getId());
		customerDto.setBusinessName(customer.getBusinessName());
		customerDto.setFiscalCode(customer.getFiscalCode());
		customerDto.setLastname(customer.getLastname());
		customerDto.setName(customer.getName());
		customerDto.setPiva(customer.getPiva());
		customerDto.setType(customer.getType());
		return customerDto;
	}

	public static Customer fromDtoToEntity(CustomerDto customerDto) {

		Customer customer = new Customer();
		customer.setId(customerDto.getId());
		customer.setBusinessName(customerDto.getBusinessName());
		customer.setFiscalCode(customerDto.getFiscalCode());
		customer.setLastname(customerDto.getLastname());
		customer.setName(customerDto.getName());
		customer.setPiva(customerDto.getPiva());
		customer.setType(customerDto.getType());
		return customer;
	}

	public static void injectFromDtoToEntity(Customer customer, CustomerDto customerDto) {

		customer.setId(customerDto.getId());
		customer.setBusinessName(customerDto.getBusinessName());
		customer.setFiscalCode(customerDto.getFiscalCode());
		customer.setLastname(customerDto.getLastname());
		customer.setName(customerDto.getName());
		customer.setPiva(customerDto.getPiva());
		customer.setType(customerDto.getType());

	}
}