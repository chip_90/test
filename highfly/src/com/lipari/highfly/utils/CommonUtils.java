package com.lipari.highfly.utils;

import java.sql.Timestamp;
import java.util.Calendar;

public class CommonUtils {

	public static Timestamp formatTimestamp(Timestamp ts) {
		
		//Trasformo il timestamp attuale in date
		Calendar cal = Calendar.getInstance();
		
		//Controllo che il timestamp abbia il giusto numero di caratteri
		cal = Calendar.getInstance();
		cal.setTime(ts);
		Long millis = cal.getTimeInMillis();
		String timeStamp = millis.toString();

		if(timeStamp.length()<13) {
			int diff = 13-timeStamp.length();
			while(diff>0) {
				timeStamp += "0";
				diff--;
			}
			
			ts = new Timestamp(millis.parseUnsignedLong(timeStamp));
		}		
		return ts;
	}
}