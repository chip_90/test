package com.lipari.highfly.utils;

import com.lipari.highfly.dto.UserDto;
import com.lipari.highfly.entities.User;

public class UserUtils {

	public static UserDto fromEntityToDto(User u) {
		
		UserDto uDto = new UserDto();
		
		uDto.setId(u.getId());
		uDto.setName(u.getName());
		uDto.setSurname(u.getSurname());
		uDto.setEmail(u.getEmail());
		uDto.setUsername(u.getUsername());
		uDto.setPassword(u.getPassword());
		uDto.setRole(RoleUtils.fromEntityToDto(u.getRole()));

		return uDto;
	}
	
	public static User fromDtoToEntity(UserDto uDto) {
		
		User u = new User();
		
		u.setId(uDto.getId());
		u.setName(uDto.getName());
		u.setSurname(uDto.getSurname());
		u.setEmail(uDto.getEmail());
		u.setUsername(uDto.getUsername());
		u.setPassword(uDto.getPassword());
		u.setRole(RoleUtils.fromDtoToEntity(uDto.getRole()));

		return u;
	}
	
	public static void injectFromDtoToEntity(User u, UserDto uDto) {
		
		u.setId(uDto.getId());
		u.setName(uDto.getName());
		u.setSurname(uDto.getSurname());
		u.setEmail(uDto.getEmail());
		u.setUsername(uDto.getUsername());
		u.setPassword(uDto.getPassword());
		u.setRole(RoleUtils.fromDtoToEntity(uDto.getRole()));
	}
}