package com.lipari.highfly.utils;

import com.lipari.highfly.dto.CarServiceDto;
import com.lipari.highfly.entities.CarService;

public class CarServiceUtils {

	public static CarServiceDto fromEntityToDto(CarService c) {
		
		CarServiceDto cDto = new CarServiceDto();
		
		cDto.setServiceId(c.getServiceId());
		cDto.setModel(c.getModel());
		cDto.setSeats(c.getSeats());
		cDto.setVehicle(c.getVehicle());
		cDto.setManufacturer(c.getManufacturer());
		cDto.setSupply(c.getSupply());
		cDto.setVehicleQuantity(c.getVehicleQuantity());

		return cDto;
	}
	
	public static CarService fromDtoToEntity(CarServiceDto cDto) {
		
		CarService c = new CarService();
		
		c.setServiceId(cDto.getServiceId());
		c.setModel(cDto.getModel());
		c.setSeats(cDto.getSeats());
		c.setVehicle(cDto.getVehicle());
		c.setManufacturer(cDto.getManufacturer());
		c.setSupply(cDto.getSupply());
		c.setVehicleQuantity(cDto.getVehicleQuantity());

		return c;
	}
	
	public static void injectFromDtoToEntity(CarService c, CarServiceDto cDto) {
		
		c.setServiceId(cDto.getServiceId());
		c.setModel(cDto.getModel());
		c.setSeats(cDto.getSeats());
		c.setVehicle(cDto.getVehicle());
		c.setManufacturer(cDto.getManufacturer());
		c.setSupply(cDto.getSupply());
		c.setVehicleQuantity(cDto.getVehicleQuantity());
	}
}