package com.lipari.highfly.utils;

import com.lipari.highfly.dto.TrainServiceDto;
import com.lipari.highfly.entities.TrainService;

public class TrainServiceUtils {

	public static TrainServiceDto fromEntityToDto(TrainService t) {
		
		TrainServiceDto tDto = new TrainServiceDto();
		
		tDto.setServiceId(t.getServiceId());
		tDto.setModel(t.getModel());
		tDto.setSeats(t.getSeats());
		tDto.setVehicle(t.getVehicle());
		tDto.setType(t.getType());
		tDto.setWagonsNumber(t.getWagonsNumber());

		return tDto;
	}
	
	public static TrainService fromDtoToEntity(TrainServiceDto tDto) {
		
		TrainService t = new TrainService();
		
		t.setServiceId(tDto.getServiceId());
		t.setModel(tDto.getModel());
		t.setSeats(tDto.getSeats());
		t.setVehicle(tDto.getVehicle());
		t.setType(tDto.getType());
		t.setWagonsNumber(tDto.getWagonsNumber());

		return t;
	}
	
	public static void injectFromDtoToEntity(TrainService t, TrainServiceDto tDto) {
		
		t.setServiceId(tDto.getServiceId());
		t.setModel(tDto.getModel());
		t.setSeats(tDto.getSeats());
		t.setVehicle(tDto.getVehicle());
		t.setType(tDto.getType());
		t.setWagonsNumber(tDto.getWagonsNumber());
	}
}