package com.lipari.highfly.utils;

import com.lipari.highfly.dto.AirplaneServiceDto;
import com.lipari.highfly.entities.AirplaneService;

public class AirplaneServiceUtils {

	public static AirplaneServiceDto fromEntityToDto(AirplaneService a) {
		
		AirplaneServiceDto aDto = new AirplaneServiceDto();
		
		aDto.setServiceId(a.getServiceId());
		aDto.setModel(a.getModel());
		aDto.setSeats(a.getSeats());
		aDto.setVehicle(a.getVehicle());
		aDto.setSeatsFirstClass(a.getSeatsFirstClass());
		aDto.setSeatsSecondClass(a.getSeatsSecondClass());

		return aDto;
	}
	
	public static AirplaneService fromDtoToEntity(AirplaneServiceDto aDto) {
		
		AirplaneService a = new AirplaneService();
		
		a.setServiceId(aDto.getServiceId());
		a.setModel(aDto.getModel());
		a.setSeats(aDto.getSeats());
		a.setVehicle(aDto.getVehicle());
		a.setSeatsFirstClass(aDto.getSeatsFirstClass());
		a.setSeatsSecondClass(aDto.getSeatsSecondClass());

		return a;
	}
	
	public static void injectFromDtoToEntity(AirplaneService a, AirplaneServiceDto aDto) {
		
		a.setServiceId(aDto.getServiceId());
		a.setModel(aDto.getModel());
		a.setSeats(aDto.getSeats());
		a.setVehicle(aDto.getVehicle());
		a.setSeatsFirstClass(aDto.getSeatsFirstClass());
		a.setSeatsSecondClass(aDto.getSeatsSecondClass());
	}
}