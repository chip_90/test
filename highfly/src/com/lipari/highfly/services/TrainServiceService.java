/**
 * UserServiceImpl.java
 *
 * robgion
 * www.2clever.it
 * 
 * 05 lug 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.services;

import java.util.List;

import com.lipari.highfly.dto.TrainServiceDto;

/**
 * @author chip_90
 *
 */

public interface TrainServiceService {
	
	TrainServiceDto nuovoTrainService(TrainServiceDto tDto);
	
	TrainServiceDto aggiornaTrainService(TrainServiceDto tDto);
	
	void eliminaTrainService(Integer id);
	
	List<TrainServiceDto> listaTrainService();
	
	TrainServiceDto findById(Integer id);
}