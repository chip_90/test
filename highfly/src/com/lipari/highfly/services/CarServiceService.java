/**
 * UserServiceImpl.java
 *
 * robgion
 * www.2clever.it
 * 
 * 05 lug 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.services;

import java.util.List;

import com.lipari.highfly.dto.CarServiceDto;

/**
 * @author chip_90
 *
 */

public interface CarServiceService {
	
	CarServiceDto nuovoCarService(CarServiceDto cDto);
	
	CarServiceDto aggiornaCarService(CarServiceDto cDto);
	
	void eliminaCarService(Integer id);
	
	List<CarServiceDto> listaCarService();
	
	public boolean findByModel(String model);
	
	CarServiceDto findById(Integer id);
}