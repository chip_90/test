package com.lipari.highfly.services;

import com.lipari.highfly.dto.ServiceDto;
import com.lipari.highfly.dto.ServicesRemoteDto;

public interface ServicesService {

	ServiceDto getServiceById(int id);
	
	public ServicesRemoteDto getallServices();

}