package com.lipari.highfly.services;

import java.util.List;

import com.lipari.highfly.dto.CustomerDto;

public interface CustomerService {

	/**
	 * metodo per la ricerca di un customer tramite id
	 * 
	 * @param id
	 * @return
	 */
	CustomerDto findCustomertById(int id);

	/**
	 * metodo per l'eliminazione del customer
	 * 
	 * @param id
	 */
	void deleteCustomer(int id);

	/**
	 * update del customer
	 * 
	 * @param customerDto
	 */
	CustomerDto updateCustomer(CustomerDto customerDto);

	/**
	 * salvataggio del nuovo customer
	 * 
	 * @param customerDto
	 */
	void saveCustomer(CustomerDto customerDto);

	/**
	 * metodo per l caricamneto della lista, contenente tutti i customers
	 * 
	 * @return
	 */
	List<CustomerDto> findAllCustomers();

	void softDelete(int id);

}
