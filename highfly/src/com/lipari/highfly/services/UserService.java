/**
 * UserServiceImpl.java
 *
 * robgion
 * www.2clever.it
 * 
 * 05 lug 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.services;

import java.util.List;

import com.lipari.highfly.dto.UserDto;

/**
 * @author chip_90
 *
 */

public interface UserService {
	
	UserDto autorizzazione(String username, String password);
	
	UserDto nuovoUtente(UserDto uDto);
	
	UserDto aggiornaUtente(UserDto uDto);
	
	void eliminaUtente(int id);
	
	List<UserDto> listaUtenti();
	
	List<UserDto> listaUtentiFiltrati(String filter);
	
	boolean findByUsername(String username);
	
	UserDto findById(int id);
	
	/*Utente authorizationApplication(String username, String password);	 */
}