package com.lipari.highfly.services;

import java.util.List;

import com.lipari.highfly.dto.BookingDetailDto;
import com.lipari.highfly.dto.BookingDto;

public interface BookingDetailService {

	List<BookingDetailDto> getAllDetailForBooking(int id);

	BookingDetailDto getDetailForBooking(int id, int idd);

	/*List<BookingDetailDto> getAllDetailForBookingByCode(String code);*/

	BookingDetailDto insert(BookingDetailDto bookingDetailDto);

	BookingDetailDto update(BookingDetailDto bookingDetailDto);

	void delete(int id);

	BookingDetailDto insert(BookingDetailDto bookingDetailDto, BookingDto bookingDto);


	


}