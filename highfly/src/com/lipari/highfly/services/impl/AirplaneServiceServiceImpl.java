/**
 * UserServiceImpl.java
 *
 * robgion
 * www.2clever.it
 * 
 * 05 lug 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lipari.highfly.controllers.AirplaneServiceController;
import com.lipari.highfly.dao.AirplaneServiceDao;
import com.lipari.highfly.dto.AirplaneServiceDto;
import com.lipari.highfly.entities.AirplaneService;
import com.lipari.highfly.services.AirplaneServiceService;
import com.lipari.highfly.utils.AirplaneServiceUtils;

/**
 * @author chip_90
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class AirplaneServiceServiceImpl implements AirplaneServiceService {
	
	private static final Logger logger = Logger.getLogger(AirplaneServiceController.class);

	@Autowired
	private AirplaneServiceDao airDao;
	
	public AirplaneServiceDto nuovoAirplaneService(AirplaneServiceDto aDto) {	
		
		AirplaneService a = AirplaneServiceUtils.fromDtoToEntity(aDto);
		
		a = airDao.create(a);
		
		return aDto;
	}
	
	public AirplaneServiceDto aggiornaAirplaneService(AirplaneServiceDto aDto) {
		
		AirplaneService a = airDao.find(aDto.getServiceId());
				
		if(a!=null){
			
			AirplaneServiceUtils.injectFromDtoToEntity(a, aDto);		
			airDao.update(a);
			return aDto;
		}
		
		return null;
	}
	
	public void eliminaAirplaneService(Integer id) {
		
		airDao.delete(id);
	}
	
	public List<AirplaneServiceDto> listaAirplaneService() {
		
		List<AirplaneService> listAirplane = airDao.findAll();
		
		 if(listAirplane!=null && !listAirplane.isEmpty()) {
		
			logger.info("Lista aerei trovata. Attendere...");
			List<AirplaneServiceDto> lAirplaneDto = new ArrayList<AirplaneServiceDto>();
			
			for (AirplaneService aereo : listAirplane) {
							
				AirplaneServiceDto airplane = AirplaneServiceUtils.fromEntityToDto(aereo);
				
				lAirplaneDto.add(airplane);
			}
	
			logger.info("Lista aerei caricata.");
			return lAirplaneDto;
		 }
		 logger.info("Lista aerei vuota!");
		 
		 return null;
	}
	
	public AirplaneServiceDto findById(Integer id) {

		AirplaneService aereo = airDao.find(id);
		
		 if(aereo != null) {		
			 logger.info("Aereo trovato.");
			 return AirplaneServiceUtils.fromEntityToDto(aereo);
		 }  
		 logger.info("Aereo non trovato.");
		 
		 return null;
	}
}