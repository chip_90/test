package com.lipari.highfly.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lipari.highfly.dao.BookingDetailDao;
import com.lipari.highfly.dto.BookingDetailDto;
import com.lipari.highfly.dto.BookingDto;
import com.lipari.highfly.dto.CarServiceDto;
import com.lipari.highfly.dto.ServiceDto;
import com.lipari.highfly.entities.BookingDetail;
import com.lipari.highfly.services.BookingDetailService;
import com.lipari.highfly.services.CarServiceService;
import com.lipari.highfly.services.ServicesService;
import com.lipari.highfly.utils.BookingDetailUtils;
import com.lipari.highfly.utils.CommonUtils;
import com.lipari.highfly.utils.CustomQueryFilter;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class BookingDetailServiceImpl implements BookingDetailService {

	@Autowired
	private BookingDetailDao bookingDetailDao;
	
	@Autowired
	private ServicesService	servicesService;
	
	@Autowired
	private CarServiceService carService;

	@Override
	public List<BookingDetailDto> getAllDetailForBooking(int id) {
		
		List<BookingDetail> lista = bookingDetailDao.getAllDetailForBooking(id);
		
		List<BookingDetailDto> result = new ArrayList<BookingDetailDto>();
		
		lista.stream().forEach(
				b -> {
					
					ServiceDto sDto = null;
					
					//Recupero il service
					sDto = servicesService.getServiceById(b.getService().getServiceId());
					
					result.add(new BookingDetailDto
						(
						b.getId(), 
						b.getDepartureDate(), 
						b.getDeparturePlace(), 
						b.getDestinationPlace(), 
						sDto
						));
				}
		);
		return result;
	}

	@Override
	public BookingDetailDto getDetailForBooking(int id, int idd) {

		BookingDetail detail = bookingDetailDao.getAllDetailForBooking(id).get(0);
		
		ServiceDto sDto = null;
		
		//Recupero il service
		sDto = servicesService.getServiceById(detail.getService().getServiceId());
		
		return new BookingDetailDto
			(
			detail.getId(), 
			detail.getDepartureDate(), 
			detail.getDeparturePlace(), 
			detail.getDestinationPlace(), 
			sDto
			);
	}
				
	/*@Override

	public List<BookingDetailDto> getAllDetailForBookingByCode(String code) {
		return BookingDetailUtils.fromEntityToDto(bookingDetailDao.getAllDetailForBookingByCode(code));
	}*/

	@Override
	public BookingDetailDto insert(BookingDetailDto bookingDetailDto, BookingDto bookingDto) {
		
		if (bookingDetailDto != null) {
		
			//Recupero il service
			ServiceDto serv = servicesService.getServiceById(bookingDetailDto.getService().getServiceId());
			bookingDetailDto.setService(serv);
			
			//Controllo che ci siano ancora veicoli disponibili
			if(!this.checkIfIsFreeService(bookingDetailDto)) {
				return null;
			}
			
			//Controllo che il timestamp abbia il giusto numero di caratteri
			bookingDetailDto.setDepartureDate(CommonUtils.formatTimestamp(bookingDetailDto.getDepartureDate()));
			
			BookingDetail booking = BookingDetailUtils.fromDtoToEntity(bookingDetailDto, bookingDto);
			
			bookingDetailDao.create(booking);
		}
		return bookingDetailDto;
	}
	
	public BookingDetailDto insert(BookingDetailDto bookingDetailDto) {
		
		//Recupero il service
		ServiceDto serv = servicesService.getServiceById(bookingDetailDto.getService().getServiceId());
		bookingDetailDto.setService(serv);
		
		//Controllo che il timestamp abbia il giusto numero di caratteri
		bookingDetailDto.setDepartureDate(CommonUtils.formatTimestamp(bookingDetailDto.getDepartureDate()));
		
		//Controllo che ci siano ancora veicoli disponibili
		if(!this.checkIfIsFreeService(bookingDetailDto)) {
			return null;
		}
		
		BookingDetail booking = BookingDetailUtils.fromDtoToEntity(bookingDetailDto);
		
		bookingDetailDao.create(booking);
		return bookingDetailDto;
	}

	@Override
	public BookingDetailDto update(BookingDetailDto bookingDetailDto) {

		BookingDetail bookingDetail = bookingDetailDao.find(bookingDetailDto.getId());

		if (bookingDetail != null) {
			
			//Recupero il service
			ServiceDto serv = servicesService.getServiceById(bookingDetailDto.getService().getServiceId());
			bookingDetailDto.setService(serv);
			
			//Controllo che il timestamp abbia il giusto numero di caratteri
			bookingDetailDto.setDepartureDate(CommonUtils.formatTimestamp(bookingDetailDto.getDepartureDate()));
			
			//Controllo che ci siano ancora veicoli disponibili
			if(!this.checkIfIsFreeService(bookingDetailDto)) {
				return null;
			}
		
			BookingDetailUtils.injectFromDtoToEntity(bookingDetail, bookingDetailDto);

			bookingDetailDao.update(bookingDetail);
		}
		return bookingDetailDto;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(int id) {

		BookingDetail bookingDetail = bookingDetailDao.find(id);

		if (bookingDetail != null) {
			bookingDetailDao.delete(id);
		}
	}
	
	//Metodo che controlla se ci sono service disponibili (SOLO PER AUTO)
	private boolean checkIfIsFreeService (BookingDetailDto bookingDetailDto) {
		
		if(bookingDetailDto.getService().getVehicle().equals("Car")) {
			
			//Costruzione dei filtri
			CustomQueryFilter dateFilter = new CustomQueryFilter();
			dateFilter.setFieldName("departureDate");
			dateFilter.setFieldValue(bookingDetailDto.getDepartureDate());
			
			//Lista di tutte le auto disponibili
			List<CarServiceDto> listCarService = carService.listaCarService();
			
			//Creo una mappa per conteggiare la disponibilit� nel parco auto
			Map<Integer,Integer> carList = new HashMap<Integer, Integer>();
			
			//Scorro la lista delle auto disponibili e le inserisco nella mappa
			for (CarServiceDto carServiceDto : listCarService) {
				carList.put(carServiceDto.getServiceId(), carServiceDto.getVehicleQuantity());
			}
			
			//Lista di tutti i booking detail esistenti filtrati in base a departure_date
			List<BookingDetail> listBookingDetailFiltered = bookingDetailDao.findAllByFilters(dateFilter);
			
			//Scorro la lista, controllo solo le auto e decremento quelle non piu disponibili
			for (BookingDetail bookingDetail : listBookingDetailFiltered ) {
							
				if(bookingDetail.getService().getVehicle().equals("Car")) {
					carList.replace(bookingDetail.getService().getServiceId() , (carList.get( bookingDetail.getService().getServiceId() ) -1) );
				}
			}
			
			//Se la quantit� rimasta � 0 
			if(carList.get(bookingDetailDto.getService().getServiceId())<=0) {
				return false;
			}		
		}
		return true;	
	}
}