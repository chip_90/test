package com.lipari.highfly.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lipari.highfly.dao.CustomerDao;
import com.lipari.highfly.dto.CustomerDto;
import com.lipari.highfly.entities.Customer;
import com.lipari.highfly.services.CustomerService;
import com.lipari.highfly.utils.CustomerUtils;

@Service(value = "customerService")
@Transactional(propagation = Propagation.REQUIRED)
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	@Qualifier("customerDao")
	private CustomerDao customerDao;

	/**
	 * metodo per la ricerca di un customer tramite id
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public CustomerDto findCustomertById(int id) {
		CustomerDto customer = null;

		customer = CustomerUtils.fromEntityToDto(customerDao.find(id));

		return customer;
	}

	/**
	 * metodo per l'eliminazione del customer
	 * 
	 * @param id
	 */
	@Override
	public void deleteCustomer(int id) {
		customerDao.delete(id);
	}

	/**
	 * metodo per l'eliminazione logica del customer
	 * 
	 * @param id
	 */
	@Override
	public void softDelete(int id) {
		customerDao.softDelete(id);
	}
	
	/**
	 * update del customer
	 * 
	 * @param customerDto
	 */
	@Override
	public CustomerDto updateCustomer(CustomerDto customerDto) {

		Customer customer = customerDao.find(customerDto.getId());
		CustomerUtils.injectFromDtoToEntity(customer, customerDto);	
		customerDao.update(customer);
	

		if (customer != null) {
			return customerDto;
		} else {

			return null;
		}
	}

	/**
	 * salvataggio del nuovo customer
	 * 
	 * @param customerDto
	 */
	@Override
	public void saveCustomer(CustomerDto customerDto) {
		Customer customer = new Customer();
		CustomerUtils.injectFromDtoToEntity(customer, customerDto);
		customerDao.create(customer);
	}

	/**
	 * metodo per l caricamneto della lista, contenente tutti i customers
	 * 
	 * @return
	 */
	@Override
	public List<CustomerDto> findAllCustomers() {
		final List<CustomerDto> listDto = new ArrayList<>();
		final List<Customer> listEntity = customerDao.findAll();

		listEntity.stream().forEach(en -> listDto.add(CustomerUtils.fromEntityToDto(en)));

		return listDto;
	}
}