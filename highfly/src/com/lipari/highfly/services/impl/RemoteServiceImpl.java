package com.lipari.highfly.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lipari.highfly.dto.BookingDto;
import com.lipari.highfly.services.BookingDetailService;
import com.lipari.highfly.services.BookingService;
import com.lipari.highfly.services.CustomerService;
import com.lipari.highfly.services.RemoteService;
import com.lipari.highfly.utils.CommonUtils;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class RemoteServiceImpl implements RemoteService {
	
	@Autowired
	private BookingService bookingService;
	
	@Autowired
	private BookingDetailService bookingDetailService;
	
	@Autowired
	private CustomerService	customerService;
	
	public String[] newBookingRemote(BookingDto bookingDto) {
		
		//Inserisco forzatamente l'utente
		//TODO: successivamente eliminare e inserire l'utente autenticato
		bookingDto.setCustomer(customerService.findCustomertById(3));

		BookingDto newBooking = bookingService.saveBooking(bookingDto);
		
		//BookingDetailDto bookingDetailDto: bookingDto.getBookingDetails())
		String elementiEsclusi = "";
		
		for(int i = 0; i<bookingDto.getBookingDetails().size(); i++) {
			
			if(bookingDetailService.insert(bookingDto.getBookingDetails().get(i), newBooking)==null) {
				elementiEsclusi += "\n Data partenza: "+ CommonUtils.formatTimestamp(bookingDto.getBookingDetails().get(i).getDepartureDate())
				+" \n Luogo di partenza: "+ bookingDto.getBookingDetails().get(i).getDeparturePlace()
				+" \n Logo di arrivo: "+ bookingDto.getBookingDetails().get(i).getDestinationPlace()
				+" \n Mezzo richiesto: "+ bookingDto.getBookingDetails().get(i).getService().getModel()
				+" \n --------------------------------------------------------------";
			}
		}
	
		String[] response = new String[2];
		response[0] = bookingDto.getCode();
		response[1] = elementiEsclusi;
		return response;
	}
}