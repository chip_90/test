/**
 * UserServiceImpl.java
 *
 * robgion
 * www.2clever.it
 * 
 * 05 lug 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lipari.highfly.controllers.TrainServiceController;
import com.lipari.highfly.dao.TrainServiceDao;
import com.lipari.highfly.dto.TrainServiceDto;
import com.lipari.highfly.entities.TrainService;
import com.lipari.highfly.services.TrainServiceService;
import com.lipari.highfly.utils.TrainServiceUtils;

/**
 * @author chip_90
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class TrainServiceServiceImpl implements TrainServiceService {
	
	private static final Logger logger = Logger.getLogger(TrainServiceController.class);

	@Autowired
	private TrainServiceDao trainDao;
	
	public TrainServiceDto nuovoTrainService(TrainServiceDto tDto) {	
		
		TrainService t = TrainServiceUtils.fromDtoToEntity(tDto);
		
		t = trainDao.create(t);
		
		return tDto;
	}
	
	public TrainServiceDto aggiornaTrainService(TrainServiceDto tDto) {
		
		TrainService t = trainDao.find(tDto.getServiceId());
				
		if(t!=null){
			
			TrainServiceUtils.injectFromDtoToEntity(t, tDto);		
			trainDao.update(t);
			return tDto;
		}
		
		return null;
	}
	
	public void eliminaTrainService(Integer id) {
		
		trainDao.delete(id);
	}
	
	public List<TrainServiceDto> listaTrainService() {
		
		List<TrainService> listaTrain = trainDao.findAll();
		
		 if(listaTrain!=null && !listaTrain.isEmpty()) {
		
			logger.info("Lista treni trovata. Attendere...");
			List<TrainServiceDto> lTrainDto = new ArrayList<TrainServiceDto>();
			
			for (TrainService treno : listaTrain) {
							
				TrainServiceDto train = TrainServiceUtils.fromEntityToDto(treno);
				
				lTrainDto.add(train);
			}
	
			logger.info("Lista treni caricata.");
			return lTrainDto;
		 }
		 logger.info("Lista treni vuota!");
		 
		 return null;
	}
	
	public TrainServiceDto findById(Integer id) {

		TrainService treno = trainDao.find(id);
		
		 if(treno != null) {		
			 logger.info("Treno trovato.");
			 return TrainServiceUtils.fromEntityToDto(treno);
		 }  
		 logger.info("Treno non trovato.");
		 
		 return null;
	}
}