/**
 * UserServiceImpl.java
 *
 * robgion
 * www.2clever.it
 * 
 * 05 lug 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lipari.highfly.controllers.RoleController;
import com.lipari.highfly.dao.RoleDao;
import com.lipari.highfly.dto.RoleDto;
import com.lipari.highfly.entities.Role;
import com.lipari.highfly.services.RoleService;
import com.lipari.highfly.utils.CustomQueryFilter;
import com.lipari.highfly.utils.RoleUtils;

/**
 * @author chip_90
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class RoleServiceImpl implements RoleService {
	
	private static final Logger logger = Logger.getLogger(RoleController.class);
	
	@Autowired
	private RoleDao roleDao;
	
	public boolean findByCode(String code) {
		
		// Costruzione dei filtri
		CustomQueryFilter codeFilter = new CustomQueryFilter();
		codeFilter.setFieldName("code");
		codeFilter.setFieldValue(code);

		List<Role> roles = roleDao.findAllByFilters(codeFilter);
		
		 if(roles != null && !roles.isEmpty()) {
			 
			 if(roles.size() > 1)
				 logger.info("Too many roles");
			
			 return true;	
		 }  
		 logger.info("Ruolo non trovato.");
		 
		 return false;
	}
	
	public RoleDto nuovoRuolo(RoleDto rDto) {	
		
		Role r = RoleUtils.fromDtoToEntity(rDto);
		r = roleDao.create(r);
		
		return rDto;
	}
	
	public RoleDto aggiornaRuolo(RoleDto rDto) {
		
		Role r = roleDao.find(rDto.getId());
				
		if(r!=null){
			
			RoleUtils.injectFromDtoToEntity(r, rDto);		
			roleDao.update(r);
			return rDto;
		}
		
		return null;	
	}
	
	public void eliminaRuolo(int id) {
		
		roleDao.delete(id);
	}
	
	public List<RoleDto> listaRuoli() {
		
		List<Role> listaRuoli = roleDao.findAll();
		
		 if(listaRuoli!=null && !listaRuoli.isEmpty()) {
		
			logger.info("Lista ruoli trovata. Attendere...");
			List<RoleDto> lRoleDto = new ArrayList<RoleDto>();
			
			for (Role ruolo : listaRuoli) {
							
				RoleDto role = RoleUtils.fromEntityToDto(ruolo);
				
				lRoleDto.add(role);
			}
	
			logger.info("Lista utenti caricata.");
			return lRoleDto;
		 }
		 logger.info("Lista utenti vuota!");
		 
		 return null;
	}
	
	public List<RoleDto> listaRuoliFiltrati(String filter) {
		
		// Costruzione dei filtri
		CustomQueryFilter nomeFilter = new CustomQueryFilter();
		nomeFilter.setFieldName("description");
		nomeFilter.setFieldValue(filter);
		
		CustomQueryFilter cognomeFilter = new CustomQueryFilter();
		cognomeFilter.setFieldName("code");
		cognomeFilter.setFieldValue(filter);

		List<Role> ruoli = roleDao.findByFilteredInLike(nomeFilter, cognomeFilter);
		
		if(ruoli != null && !ruoli.isEmpty()) {
			
			logger.info("Lista ruoli trovata. Attendere...");
			List<RoleDto> lRuoliDto = new ArrayList<RoleDto>();
			
			for (Role ruolo : ruoli) {
				
				RoleDto role = RoleUtils.fromEntityToDto(ruolo);
				
				lRuoliDto.add(role);
			}
			logger.info("Lista ruoli caricata.");
			return lRuoliDto;
		}
		logger.info("Lista ruoli vuota!");
		
		return null;
	}
	
	public RoleDto findById(int id) {

		Role ruolo = roleDao.find((Integer)id);
		
		 if(ruolo != null) {		
			 logger.info("Ruolo trovato.");
			 return RoleUtils.fromEntityToDto(ruolo);
		 }  
		 logger.info("Ruolo non trovato.");
		 
		 return null;
	}
}