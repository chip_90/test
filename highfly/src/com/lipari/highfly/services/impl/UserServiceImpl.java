/**
 * UserServiceImpl.java
 *
 * robgion
 * www.2clever.it
 * 
 * 05 lug 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lipari.highfly.controllers.UserController;
import com.lipari.highfly.dao.UserDao;
import com.lipari.highfly.dto.UserDto;
import com.lipari.highfly.entities.User;
import com.lipari.highfly.services.UserService;
import com.lipari.highfly.utils.CustomQueryFilter;
import com.lipari.highfly.utils.UserUtils;

/**
 * @author chip_90
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UserServiceImpl implements UserService {
	
	private static final Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	@Qualifier("userDaoImpl")
	private UserDao userDao;
	
	public boolean findByUsername(String username) {
		
		// Costruzione dei filtri
		CustomQueryFilter usernameFilter = new CustomQueryFilter();
		usernameFilter.setFieldName("username");
		usernameFilter.setFieldValue(username);

		List<User> users = userDao.findAllByFilters(usernameFilter);
		
		 if(users != null && !users.isEmpty()) {
			 
			 if(users.size() > 1)
				 logger.info("Too many users");
			
			 return true;	
		 }  
		 logger.info("Utente non trovato.");
		 
		 return false;
	}
	
	public UserDto nuovoUtente(UserDto uDto) {	
		
		User u = UserUtils.fromDtoToEntity(uDto);
		
		u = userDao.create(u);
		
		return uDto;
	}
	
	public UserDto aggiornaUtente(UserDto uDto) {
		
		User u = userDao.find(uDto.getId());
				
		if(u!=null){
			
			UserUtils.injectFromDtoToEntity(u, uDto);		
			userDao.update(u);
			return uDto;
		}
		
		return null;
	}
	
	public void eliminaUtente(int id) {
		
		userDao.delete(id);
	}
	
	public List<UserDto> listaUtenti() {
		
		List<User> listaUtenti = userDao.findAll();
		
		 if(listaUtenti!=null && !listaUtenti.isEmpty()) {
		
			logger.info("Lista utenti trovata. Attendere...");
			List<UserDto> lUDto = new ArrayList<UserDto>();
			
			for (User utente : listaUtenti) {
							
				UserDto user = UserUtils.fromEntityToDto(utente);
				
				lUDto.add(user);
			}
	
			logger.info("Lista utenti caricata.");
			return lUDto;
		 }
		 logger.info("Lista utenti vuota!");
		 
		 return null;
	}
	
	public List<UserDto> listaUtentiFiltrati(String filter) {
		
		// Costruzione dei filtri
		CustomQueryFilter nomeFilter = new CustomQueryFilter();
		nomeFilter.setFieldName("name");
		nomeFilter.setFieldValue(filter);
		
		CustomQueryFilter cognomeFilter = new CustomQueryFilter();
		cognomeFilter.setFieldName("surname");
		cognomeFilter.setFieldValue(filter);
		
		CustomQueryFilter usernameFilter = new CustomQueryFilter();
		usernameFilter.setFieldName("username");
		usernameFilter.setFieldValue(filter);

		List<User> users = userDao.findByFilteredInLike(nomeFilter, cognomeFilter, usernameFilter);
		
		if(users != null && !users.isEmpty()) {
			
			logger.info("Lista utenti trovata. Attendere...");
			List<UserDto> lUDto = new ArrayList<UserDto>();
			
			for (User utente : users) {
				
				UserDto user = UserUtils.fromEntityToDto(utente);
				
				lUDto.add(user);
			}
			logger.info("Lista utenti caricata.");
			return lUDto;
		}
		logger.info("Lista utenti vuota!");
		
		return null;
	}
	
	public UserDto findById(int id) {

		User user = userDao.find((Integer)id);
		
		 if(user != null) {		
			 logger.info("Utente trovato.");
			 return UserUtils.fromEntityToDto(user);
		 }  
		 logger.info("Utente non trovato.");
		 
		 return null;
	}
	
	public UserDto autorizzazione(String username, String password) {
		
		// Costruzione dei filtri
		CustomQueryFilter usernameFilter = new CustomQueryFilter();
		usernameFilter.setFieldName("username");
		usernameFilter.setFieldValue(username);

		CustomQueryFilter passwordFilter = new CustomQueryFilter();
		passwordFilter.setFieldName("password");
		passwordFilter.setFieldValue(password);

		List<User> users = userDao.findAllByFilters(usernameFilter, passwordFilter);
		
		UserDto uDto = null;
		
		if(users != null && !users.isEmpty()) {
			 
			 if(users.size() > 1)
				 logger.info("Too many users");
			 
			 uDto = new UserDto();
			 
			 for (User user : users) {
					
					uDto = UserUtils.fromEntityToDto(user);
					uDto.setPassword(null);
			}
		 }
		 else {
			logger.info("Utente non trovato.");
		 }
		 
		 return uDto;
	}
}