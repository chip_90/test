package com.lipari.highfly.services.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lipari.highfly.dao.AirplaneServiceDao;
import com.lipari.highfly.dao.BookingDao;
import com.lipari.highfly.dao.CarServiceDao;
import com.lipari.highfly.dao.TrainServiceDao;
import com.lipari.highfly.dto.BookingDto;
import com.lipari.highfly.entities.Booking;
import com.lipari.highfly.entities.BookingDetail;
import com.lipari.highfly.services.BookingService;
import com.lipari.highfly.utils.BookingUtils;
import com.lipari.highfly.utils.CommonUtils;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class BookingServiceImpl implements BookingService {

	private static final Logger logger = Logger.getLogger(BookingServiceImpl.class);

	@Autowired
	private BookingDao bookingDao;
	
	@Autowired
	private CarServiceDao carService;
	
	@Autowired
	private AirplaneServiceDao airService;
	
	@Autowired
	private TrainServiceDao trainService;	
	
	@Override
	public BookingDto saveBooking(BookingDto bookingDto) {
		
		//Inserisco lo stato in lavorazione
		bookingDto.setState("WIP");
		
		//Trasformo il timestamp attuale in date
		Calendar cal = Calendar.getInstance();
		Long ms = cal.getTimeInMillis();
		
		/*Integer anno = cal.get(cal.YEAR);
		Integer mese = cal.get(cal.MONTH)+1;
		Integer ora = cal.get(cal.HOUR_OF_DAY);
		Integer minuti = cal.get(cal.MINUTE);
		Integer giorno = cal.get(cal.DAY_OF_MONTH);*/
		
		//Creo il codice univoco per il booking e lo salvo
		String code = "c" + ms.toString();	
		bookingDto.setCode(code);

		//Controllo che il timestamp abbia il giusto numero di caratteri
		bookingDto.setDate(CommonUtils.formatTimestamp(bookingDto.getDate()));
 
		Booking booking = BookingUtils.fromDtoToEntity(bookingDto);

		Booking newBooking = bookingDao.create(booking);
		bookingDto.setId(newBooking.getId());
		
		return bookingDto;
	}

	@Override
	public BookingDto updateBooking(BookingDto bookingDto) {
		
		Booking booking = bookingDao.find(bookingDto.getId());
		
		if(booking != null) {
			
			BookingUtils.injectFromDtoToEntity(booking, bookingDto);
		}
		return bookingDto;
	}

	@Override
	public List<BookingDto> findAllBooking() throws Exception {
		
		List<Booking> listBooking = bookingDao.findAll();
		
		if(listBooking != null && !listBooking.isEmpty()) {
			List<BookingDto> lBDto = new ArrayList<BookingDto>();
			
			for(Booking booking : listBooking) {
				for(BookingDetail bt: booking.getBookingDetails()) {	
					
					if(bt.getService().getVehicle().equals("Car")) {
						bt.setService(carService.find(bt.getService().getServiceId()));
					}
					else if(bt.getService().getVehicle().equals("Train")) {
						bt.setService(trainService.find(bt.getService().getServiceId()));
					}
					else if(bt.getService().getVehicle().equals("Airplane")) {	
						bt.setService(airService.find(bt.getService().getServiceId()));
					}
				}
				BookingDto bookingDto = BookingUtils.fromEntityToDto(booking);
				lBDto.add(bookingDto);
			}
			logger.info("Lista booking caricata.");
			return lBDto;
		}
		logger.info("Lista booking vuota!");
		return null;
	}

	@Override
	public BookingDto findBooking(int id) throws Exception {
		Booking booking = bookingDao.find(id);
		if(booking != null) {			
				for(BookingDetail bt: booking.getBookingDetails()) {
					if(bt.getService().getVehicle().equals("Car")) {
						bt.setService(carService.find(bt.getService().getServiceId()));
					}
					else if(bt.getService().getVehicle().equals("Train")) {
						bt.setService(trainService.find(bt.getService().getServiceId()));
					}
					else if(bt.getService().getVehicle().equals("Airplane")) {	
						bt.setService(airService.find(bt.getService().getServiceId()));
					}	
				}
				logger.info("Lista booking caricata.");
				return BookingUtils.fromEntityToDto(booking); 
		}
		logger.info("Lista booking vuota!");
		return null;
	}

	@Override
	public void deleteBooking(int id) {
		
		bookingDao.delete(id);
	}	
}