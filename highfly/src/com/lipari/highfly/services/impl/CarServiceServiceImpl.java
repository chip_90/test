/**
 * UserServiceImpl.java
 *
 * robgion
 * www.2clever.it
 * 
 * 05 lug 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lipari.highfly.controllers.CarServiceController;
import com.lipari.highfly.dao.CarServiceDao;
import com.lipari.highfly.dto.CarServiceDto;
import com.lipari.highfly.entities.CarService;
import com.lipari.highfly.services.CarServiceService;
import com.lipari.highfly.utils.CarServiceUtils;
import com.lipari.highfly.utils.CustomQueryFilter;

/**
 * @author chip_90
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class CarServiceServiceImpl implements CarServiceService {
	
	private static final Logger logger = Logger.getLogger(CarServiceController.class);

	@Autowired
	private CarServiceDao carDao;
	
	public boolean findByModel(String model) {
		
		// Costruzione dei filtri
		CustomQueryFilter modelFilter = new CustomQueryFilter();
		modelFilter.setFieldName("model");
		modelFilter.setFieldValue(model);

		List<CarService> cars = carDao.findAllByFilters(modelFilter);
		
		 if(cars != null && !cars.isEmpty()) {
			 
			 if(cars.size() > 1)
				 logger.info("Too many cars");
			
			 return true;	
		 }  
		 logger.info("Auto non trovata.");
		 
		 return false;
	}
	
	public CarServiceDto nuovoCarService(CarServiceDto cDto) {	
		
		CarService u = CarServiceUtils.fromDtoToEntity(cDto);
		
		u = carDao.create(u);
		
		return cDto;
	}
	
	public CarServiceDto aggiornaCarService(CarServiceDto cDto) {
		
		CarService c = carDao.find(cDto.getServiceId());
				
		if(c!=null){
			
			CarServiceUtils.injectFromDtoToEntity(c, cDto);		
			carDao.update(c);
			return cDto;
		}
		
		return null;
	}
	
	public void eliminaCarService(Integer id) {
		
		carDao.delete(id);
	}
	
	public List<CarServiceDto> listaCarService() {
		
		List<CarService> listaCar = carDao.findAll();
		
		 if(listaCar!=null && !listaCar.isEmpty()) {
		
			logger.info("Lista macchine trovata. Attendere...");
			List<CarServiceDto> lCarDto = new ArrayList<CarServiceDto>();
			
			for (CarService auto : listaCar) {
							
				CarServiceDto car = CarServiceUtils.fromEntityToDto(auto);
				
				lCarDto.add(car);
			}
	
			logger.info("Lista auto caricata.");
			return lCarDto;
		 }
		 logger.info("Lista auto vuota!");
		 
		 return null;
	}
	
	public CarServiceDto findById(Integer id) {

		CarService auto = carDao.find(id);
		
		 if(auto != null) {		
			 logger.info("Auto trovata.");
			 return CarServiceUtils.fromEntityToDto(auto);
		 }  
		 logger.info("Auto non trovata.");
		 
		 return null;
	}
}