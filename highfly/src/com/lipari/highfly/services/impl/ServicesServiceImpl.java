package com.lipari.highfly.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lipari.highfly.dao.AirplaneServiceDao;
import com.lipari.highfly.dao.CarServiceDao;
import com.lipari.highfly.dao.ServicesDao;
import com.lipari.highfly.dao.TrainServiceDao;
import com.lipari.highfly.dto.ServiceDto;
import com.lipari.highfly.dto.ServicesRemoteDto;
import com.lipari.highfly.services.AirplaneServiceService;
import com.lipari.highfly.services.CarServiceService;
import com.lipari.highfly.services.ServicesService;
import com.lipari.highfly.services.TrainServiceService;
import com.lipari.highfly.utils.AirplaneServiceUtils;
import com.lipari.highfly.utils.CarServiceUtils;
import com.lipari.highfly.utils.TrainServiceUtils;

@Service(value = "servicesService")
@Transactional(propagation = Propagation.REQUIRED)
public class ServicesServiceImpl implements ServicesService {
	
	@Autowired
	private ServicesDao serviceDao;
	
	@Autowired
	private CarServiceDao carDao;
	
	@Autowired
	private TrainServiceDao trainDao;
	
	@Autowired
	private AirplaneServiceDao airplaneDao;
	
	@Autowired
	private TrainServiceService trainService;
	
	@Autowired
	private CarServiceService carService;

	@Autowired
	private AirplaneServiceService airplaneService;
	
	@Override
	public ServicesRemoteDto getallServices() {
		ServicesRemoteDto result = new ServicesRemoteDto();
		result.setTrain(trainService.listaTrainService());
		result.setCar(carService.listaCarService());
		result.setAirplane(airplaneService.listaAirplaneService());
		return result;
	}
	
	@Override
	public ServiceDto getServiceById(int id) {
			
		com.lipari.highfly.entities.Service service = serviceDao.find(id);
		
		ServiceDto sDto = null;
		
		if(service.getVehicle().equals("Car")) {
			
			sDto = CarServiceUtils.fromEntityToDto(carDao.find(id));
		}
		else if(service.getVehicle().equals("Train")) {
			
			sDto = TrainServiceUtils.fromEntityToDto(trainDao.find(id));
		}
		else if(service.getVehicle().equals("Airplane")) {
			
			sDto = AirplaneServiceUtils.fromEntityToDto(airplaneDao.find(id));
		}
					
		return sDto;
	}	
}