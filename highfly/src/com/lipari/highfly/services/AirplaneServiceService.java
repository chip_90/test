/**
 * UserServiceImpl.java
 *
 * robgion
 * www.2clever.it
 * 
 * 05 lug 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.services;

import java.util.List;

import com.lipari.highfly.dto.AirplaneServiceDto;

/**
 * @author chip_90
 *
 */

public interface AirplaneServiceService {
	
	AirplaneServiceDto nuovoAirplaneService(AirplaneServiceDto aDto);
	
	AirplaneServiceDto aggiornaAirplaneService(AirplaneServiceDto aDto);
	
	void eliminaAirplaneService(Integer id);
	
	List<AirplaneServiceDto> listaAirplaneService();
	
	AirplaneServiceDto findById(Integer id);
}