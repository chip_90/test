package com.lipari.highfly.services;

import com.lipari.highfly.dto.BookingDto;

public interface RemoteService {

	String[] newBookingRemote(BookingDto bookingDto) ;
}