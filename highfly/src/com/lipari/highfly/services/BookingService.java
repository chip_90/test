package com.lipari.highfly.services;

import java.util.List;

import com.lipari.highfly.dto.BookingDto;

public interface BookingService {
	
	BookingDto saveBooking(BookingDto bookingDto);

	BookingDto updateBooking(BookingDto bookingDto);

	List<BookingDto> findAllBooking() throws Exception;

	BookingDto findBooking(int id) throws Exception;

	void deleteBooking(int id);
		
}