/**
 * UserServiceImpl.java
 *
 * robgion
 * www.2clever.it
 * 
 * 05 lug 2017
 * For further information please write to info@2clever.it
 */
package com.lipari.highfly.services;

import java.util.List;

import com.lipari.highfly.dto.RoleDto;

/**
 * @author chip_90
 *
 */

public interface RoleService {
	
	boolean findByCode(String code);
	
	RoleDto nuovoRuolo(RoleDto uDto);
	
	RoleDto aggiornaRuolo(RoleDto uDto);
	
	void eliminaRuolo(int id);
	
	List<RoleDto> listaRuoli();
	
	List<RoleDto> listaRuoliFiltrati(String filter);
	
	RoleDto findById(int id);

}