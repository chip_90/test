package com.lipari.highfly.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author chip_90
 *
 */

@JsonTypeName("Car")
public class CarServiceDto extends ServiceDto implements Serializable {
	
	private static final long serialVersionUID = 4743494505313399336L;
	
	private String manufacturer;
	private String supply;
	private int vehicleQuantity;

	public CarServiceDto() {
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getSupply() {
		return supply;
	}

	public void setSupply(String supply) {
		this.supply = supply;
	}

	public int getVehicleQuantity() {
		return vehicleQuantity;
	}

	public void setVehicleQuantity(int vehicleQuantity) {
		this.vehicleQuantity = vehicleQuantity;
	}
}