package com.lipari.highfly.dto;

import java.sql.Timestamp;

public class BookingDetailDto {

	private int id;
	private Timestamp departureDate;
	private String departurePlace;
	private String destinationPlace;
	private ServiceDto service;

	public BookingDetailDto() {
		
	}
	
	public BookingDetailDto(int id, Timestamp departureDate, String departurePlace, String destinationPlace,
			ServiceDto service ) {

		this.id = id;
		this.departureDate = departureDate;
		this.departurePlace = departurePlace;
		this.destinationPlace = destinationPlace;
		this.service = service;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}

	public String getDeparturePlace() {
		return departurePlace;
	}

	public void setDeparturePlace(String departurePlace) {
		this.departurePlace = departurePlace;
	}

	public String getDestinationPlace() {
		return destinationPlace;
	}

	public void setDestinationPlace(String destinationPlace) {
		this.destinationPlace = destinationPlace;
	}

	public ServiceDto getService() {
		return service;
	}

	public void setService(ServiceDto service) {
		this.service = service;
	}
}