package com.lipari.highfly.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author chip_90
 *
 */
public class RoleDto implements Serializable {

	private static final long serialVersionUID = -3284934398433500824L;

	private int id;
	private String code;
	private String description;
	private List<UserDto> users;

	public RoleDto() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<UserDto> getUsers() {
		return users;
	}

	public void setUsers(List<UserDto> users) {
		this.users = users;
	}

}