package com.lipari.highfly.dto;

import java.util.List;

public class ServicesRemoteDto {
	private List<CarServiceDto> car;
	private List<TrainServiceDto> train;
	private List<AirplaneServiceDto> airplane;
	
	public ServicesRemoteDto() {
	}

	public List<CarServiceDto> getCar() {
		return car;
	}

	public void setCar(List<CarServiceDto> car) {
		this.car = car;
	}

	public List<TrainServiceDto> getTrain() {
		return train;
	}

	public void setTrain(List<TrainServiceDto> list) {
		this.train = list;
	}

	public List<AirplaneServiceDto> getAirplane() {
		return airplane;
	}

	public void setAirplane(List<AirplaneServiceDto> airplane) {
		this.airplane = airplane;
	}
	
}