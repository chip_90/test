package com.lipari.highfly.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 * @author chip_90
 *
 */
@JsonTypeInfo(use = Id.NAME, include = As.EXISTING_PROPERTY, property = "vehicle", visible = true)
@JsonSubTypes({
	  @JsonSubTypes.Type(value = AirplaneServiceDto.class, name = "Airplane"),
	  @JsonSubTypes.Type(value = CarServiceDto.class, name = "Car"),
	  @JsonSubTypes.Type(value = TrainServiceDto.class, name = "Train")
})
public abstract class ServiceDto implements Serializable {

	private static final long serialVersionUID = 4643811531251884983L;
	
	private Integer serviceId;
	private String model;
	private Integer seats;
	private String vehicle;

	public ServiceDto() {
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Integer getSeats() {
		return seats;
	}

	public void setSeats(Integer seats) {
		this.seats = seats;
	}

	public String getVehicle() {
		return vehicle;
	}

	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}
}