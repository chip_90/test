package com.lipari.highfly.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class BookingDto implements Serializable{

	private static final long serialVersionUID = 64996688068570479L;
	
	private int id;
	private String code;
	private Timestamp date;
	private String nominative;
	private int nPartecipants;
	private String state;
	private CustomerDto customer;
	private List<BookingDetailDto> bookingDetails;
	
	public BookingDto() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getNominative() {
		return nominative;
	}

	public void setNominative(String nominative) {
		this.nominative = nominative;
	}

	public int getnPartecipants() {
		return nPartecipants;
	}

	public void setnPartecipants(int nPartecipants) {
		this.nPartecipants = nPartecipants;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public CustomerDto getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDto customer) {
		this.customer = customer;
	}

	public List<BookingDetailDto> getBookingDetails() {
		return bookingDetails;
	}

	public void setBookingDetails(List<BookingDetailDto> bookingDetails) {
		this.bookingDetails = bookingDetails;
	}

}