package com.lipari.highfly.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author chip_90
 *
 */

@JsonTypeName("Train")
public class TrainServiceDto extends ServiceDto implements Serializable {
	
	private static final long serialVersionUID = -932847597399050606L;
	
	private String type;
	private Integer wagonsNumber;
	
	public TrainServiceDto() {
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getWagonsNumber() {
		return wagonsNumber;
	}

	public void setWagonsNumber(Integer wagonsNumber) {
		this.wagonsNumber = wagonsNumber;
	}
}