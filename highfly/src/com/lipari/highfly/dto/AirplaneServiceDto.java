package com.lipari.highfly.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author chip_90
 *
 */

@JsonTypeName("Airplane")
public class AirplaneServiceDto extends ServiceDto implements Serializable {

	private static final long serialVersionUID = 4643811531251884983L;
	
	private Integer seatsFirstClass;
	private Integer seatsSecondClass;
	
	public AirplaneServiceDto() {
	}

	public Integer getSeatsFirstClass() {
		return seatsFirstClass;
	}

	public void setSeatsFirstClass(Integer seatsFirstClass) {
		this.seatsFirstClass = seatsFirstClass;
	}

	public Integer getSeatsSecondClass() {
		return seatsSecondClass;
	}

	public void setSeatsSecondClass(Integer seatsSecondClass) {
		this.seatsSecondClass = seatsSecondClass;
	}
}