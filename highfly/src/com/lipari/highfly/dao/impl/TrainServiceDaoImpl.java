package com.lipari.highfly.dao.impl;

import org.springframework.stereotype.Repository;

import com.lipari.highfly.dao.TrainServiceDao;
import com.lipari.highfly.entities.TrainService;


@Repository(value="trainDaoImpl")
public class TrainServiceDaoImpl extends GenericDaoImpl<TrainService> implements TrainServiceDao {

}