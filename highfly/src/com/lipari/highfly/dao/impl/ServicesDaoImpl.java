package com.lipari.highfly.dao.impl;

import org.springframework.stereotype.Repository;

import com.lipari.highfly.dao.ServicesDao;
import com.lipari.highfly.entities.Service;

@Repository(value="serviceDao")
public class ServicesDaoImpl extends GenericDaoImpl<Service> implements ServicesDao{
	
}