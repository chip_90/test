package com.lipari.highfly.dao.impl;

import org.springframework.stereotype.Repository;

import com.lipari.highfly.dao.CarServiceDao;
import com.lipari.highfly.entities.CarService;


@Repository(value="carDaoImpl")
public class CarServiceDaoImpl extends GenericDaoImpl<CarService> implements CarServiceDao {

}