package com.lipari.highfly.dao.impl;

import org.springframework.stereotype.Repository;

import com.lipari.highfly.dao.AirplaneServiceDao;
import com.lipari.highfly.entities.AirplaneService;


@Repository(value="airplaneDaoImpl")
public class AirplaneServiceDaoImpl extends GenericDaoImpl<AirplaneService> implements AirplaneServiceDao {

}