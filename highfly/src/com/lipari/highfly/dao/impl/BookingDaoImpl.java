package com.lipari.highfly.dao.impl;

import org.springframework.stereotype.Repository;

import com.lipari.highfly.dao.BookingDao;
import com.lipari.highfly.entities.Booking;


@Repository(value="bookingDaoImpl")
public class BookingDaoImpl extends GenericDaoImpl<Booking> implements BookingDao {

}