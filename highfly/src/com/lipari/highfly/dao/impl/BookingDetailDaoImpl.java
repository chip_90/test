package com.lipari.highfly.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.lipari.highfly.dao.BookingDetailDao;
import com.lipari.highfly.entities.BookingDetail;

@Component
public class BookingDetailDaoImpl extends GenericDaoImpl<BookingDetail> implements BookingDetailDao {

	@Override
	public List<BookingDetail> getAllDetailForBooking(Integer id) {

		String sqlString = "SELECT d FROM BookingDetail d WHERE d.booking.id = :id";
		Query query = em.createQuery(sqlString);
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public List<BookingDetail> getDetailForBooking(int id, int idd) {
		String sqlString = "SELECT d FROM BookingDetail d WHERE d.booking.id = :id AND d.id=:idd";
		Query query = em.createQuery(sqlString);
		query.setParameter("id", id);
		query.setParameter("idd", idd);
		return query.getResultList();
	}

	@Override
	public List<BookingDetail> getAllDetailForBookingByCode(String code) {
		String sqlString = "SELECT d FROM BookingDetail d WHERE d.booking.code = :code";
		Query query = em.createQuery(sqlString);
		query.setParameter("code", code);
		return query.getResultList();
	}
}