package com.lipari.highfly.dao.impl;

import org.springframework.stereotype.Repository;

import com.lipari.highfly.dao.CustomerDao;
import com.lipari.highfly.entities.Customer;

@Repository(value="customerDao")
public class CustomerDaoImpl extends GenericDaoImpl<Customer> implements CustomerDao{
	
	@Override
	public void softDelete(int id) {
		Customer customer = null;
		
		customer = em.find(Customer.class, id);
		customer.setIsActive(0);
		update(customer);
	}
}