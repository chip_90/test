package com.lipari.highfly.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.lipari.highfly.dao.UserDao;
import com.lipari.highfly.entities.User;
import com.lipari.highfly.utils.CustomQueryFilter;

/**
 * @author chip_90
 *
 */

@Repository(value="userDaoImpl")
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

	/*@Override
	public List<Utente> findByUsernameAndPassword(String username, String password) {
		
		// Costruzione dei filtri
		CustomQueryFilter usernameFilter = new CustomQueryFilter();
		usernameFilter.setFieldName("username");
		usernameFilter.setFieldValue(username);

		CustomQueryFilter passwordFilter = new CustomQueryFilter();
		passwordFilter.setFieldName("password");
		passwordFilter.setFieldValue(password);

		List<Utente> users = findAllByFilters(usernameFilter, passwordFilter);

		return users;

	}*/
	
	@Override
	public List<User> findByFilteredInLike(CustomQueryFilter... filters) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(User.class);

		Root root = criteriaQuery.from(User.class);
		if (filters != null && filters.length > 0) {

			List<Predicate> predicates = new ArrayList<Predicate>();

			// Ci sono dei filtri in WHERE
			for (CustomQueryFilter filter : filters) {
				
				predicates.add(criteriaBuilder.like(root.get(filter.getFieldName()), "%"+filter.getFieldValue()+"%"));
			}

				criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[] {})));
			
		}

		criteriaQuery.select(root);

		return em.createQuery(criteriaQuery).getResultList();
	}
}