
package com.lipari.highfly.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.lipari.highfly.dao.GenericDao;
import com.lipari.highfly.utils.CustomQueryFilter;

/**
 * @author chip_90
 *
 */

public abstract class GenericDaoImpl<T> implements GenericDao<T> {

	/**
	 * Iniezione dell'EntityManager in modo automatico.
	 */
	@PersistenceContext
	protected EntityManager em;
	
	private Class<T> type;


	@PostConstruct
	public void init() {

		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class) pt.getActualTypeArguments()[0];
	}

	@Override
	public T create(T t) {
		this.em.persist(t);
		return t;
	}

	@Override
	public void delete(final Object id) {
		this.em.remove(this.em.getReference(type, id));	
	}

	@Override
	public T find(final Object id) {
		return (T) this.em.find(type, id);
	}

	@Override
	public T update(final T t) {
		return this.em.merge(t);
	}

	@Override
	public List<T> findAll() {
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(type);

		Root root = criteriaQuery.from(type);

		criteriaQuery.select(root);

		return em.createQuery(criteriaQuery).getResultList();
	}
	
	@Override
	public List<T> findAllByFilters(CustomQueryFilter... filters) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(type);

		Root root = criteriaQuery.from(type);
		if (filters != null && filters.length > 0) {

			List<Predicate> predicates = new ArrayList<Predicate>();

			// Ci sono dei filtri in WHERE
			for (CustomQueryFilter filter : filters) {
				predicates.add(criteriaBuilder.equal(root.get(filter.getFieldName()), filter.getFieldValue()));
			}
				criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[] {})));
			
		}

		criteriaQuery.select(root);

		return em.createQuery(criteriaQuery).getResultList();
	}

}