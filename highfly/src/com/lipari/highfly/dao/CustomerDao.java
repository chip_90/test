package com.lipari.highfly.dao;

import com.lipari.highfly.entities.Customer;

public interface CustomerDao extends GenericDao<Customer>{

	void softDelete(int id);
}