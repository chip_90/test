package com.lipari.highfly.dao;

import com.lipari.highfly.entities.AirplaneService;

public interface AirplaneServiceDao extends GenericDao<AirplaneService> {

}