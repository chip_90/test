package com.lipari.highfly.dao;

import com.lipari.highfly.entities.Booking;

public interface BookingDao extends GenericDao<Booking> {

}