package com.lipari.highfly.dao;

import java.util.List;

import com.lipari.highfly.entities.Role;
import com.lipari.highfly.utils.CustomQueryFilter;

/**
 * @author chip_90
 *
 */

public interface RoleDao extends GenericDao<Role> {
	
	public List<Role> findByFilteredInLike(CustomQueryFilter... filters);
}