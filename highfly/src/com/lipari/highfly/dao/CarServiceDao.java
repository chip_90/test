package com.lipari.highfly.dao;

import com.lipari.highfly.entities.CarService;

public interface CarServiceDao extends GenericDao<CarService> {

}