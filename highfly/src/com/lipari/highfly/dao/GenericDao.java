package com.lipari.highfly.dao;

import java.util.List;

import com.lipari.highfly.utils.CustomQueryFilter;

/**
 * @author chip_90
 *
 */

public interface GenericDao<T> {

    T create(T t);
    void delete(Object id);
    T find(Object id);
    T update(T t);
	List<T> findAll();   
	List<T> findAllByFilters(CustomQueryFilter... filters);
}