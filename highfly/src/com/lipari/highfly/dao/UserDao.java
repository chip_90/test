package com.lipari.highfly.dao;

import java.util.List;

import com.lipari.highfly.entities.User;
import com.lipari.highfly.utils.CustomQueryFilter;

/**
 * @author chip_90
 *
 */

public interface UserDao extends GenericDao<User> {

	//public List<Utente> findByUsernameAndPassword(String username, String password);
	public List<User> findByFilteredInLike(CustomQueryFilter... filters);
}