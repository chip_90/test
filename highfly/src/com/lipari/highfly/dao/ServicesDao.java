package com.lipari.highfly.dao;

import com.lipari.highfly.entities.Service;

public interface ServicesDao extends GenericDao<Service>{

}