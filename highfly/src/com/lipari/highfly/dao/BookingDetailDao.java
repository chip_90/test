package com.lipari.highfly.dao;

import java.util.List;

import com.lipari.highfly.entities.BookingDetail;

public interface BookingDetailDao extends GenericDao<BookingDetail>{

	List<BookingDetail> getAllDetailForBooking(Integer id);
	List<BookingDetail> getDetailForBooking(int id, int idd);
	List<BookingDetail> getAllDetailForBookingByCode(String code);

}