package com.lipari.highfly.dao;

import com.lipari.highfly.entities.TrainService;

public interface TrainServiceDao extends GenericDao<TrainService> {

}