package com.lipari.highfly.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lipari.highfly.dto.CarServiceDto;
import com.lipari.highfly.services.CarServiceService;
import com.lipari.highfly.utils.ResponseMessage;

@RestController
@RequestMapping("/api")
public class CarServiceController {
	
	private static final Logger logger = Logger.getLogger(CarServiceController.class);

	@Autowired
	private CarServiceService carService;
	
	/**
	 *  -------------------Lista Completa Auto ------------------------------
	 * @return
	 */

	@RequestMapping(value = "/carsList", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> allCarsList() {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Recupero Lista Auto...");
		try {
			rm.setData(carService.listaCarService());
			logger.info("Lista Auto recuperata.");
			rm.setCode("OK");

		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento lista non riuscito");
			logger.error("errore nel recupero della lista auto", e);

			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/**
	 *  -------------------Ricerca Auto per Id ------------------------------
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/car/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> getCar(@PathVariable("id") int id) {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Ricerca Auto...");
		try {
			//Controlliamo che l'utente esista....
			if(carService.findById(id)!=null) {
				rm.setData(carService.findById(id));
				logger.info("Auto recuperata.");
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: auto non presente");
				logger.error("ERRORE: auto non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento auto non riuscito");
			logger.error("errore nel recupero dell'auto", e);

			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
			
	/***
	 *  -------------------Eliminazione Auto ----------------------------
	 * @param id
	 * @return
	 */

	@RequestMapping(value = "/carDelete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ResponseMessage> deleteCarService(@PathVariable("id") int id) {

		logger.info("Ricerca auto da eliminare...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che l'utente esista....
			if(carService.findById(id)!=null) {
				carService.eliminaCarService(id);
				rm.setCode("OK");
				logger.info("Auto eliminato");
			}
			else {
				rm.setCode("ERRORE: auto non presente");
				logger.error("ERRORE: auto non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: eliminazione non riuscita");
			logger.error("ERRORE:eliminazione non riuscita", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
		
	/**
	 *  -------------------Aggiorna Auto ----------------------------
	 * @param car
	 * @return
	 */

	@RequestMapping(value = "/carUpdate", method = RequestMethod.PUT)
	public ResponseEntity<ResponseMessage> updateCarService(@RequestBody CarServiceDto car) {

		logger.info("Updating Car...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che l'auto esista....
			if(carService.findById(car.getServiceId())!=null) {
				rm.setData(carService.aggiornaCarService(car));
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: auto non presente");
				logger.error("ERRORE: auto non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: aggiornamento non riuscito");
			logger.error("ERRORE: aggiornamento non riuscito", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
}