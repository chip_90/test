package com.lipari.highfly.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lipari.highfly.dto.BookingDetailDto;
import com.lipari.highfly.services.BookingDetailService;
import com.lipari.highfly.utils.ResponseMessage;

@RestController
@RequestMapping("/api")
public class BookingDetailController {
	
	private static final Logger logger = Logger.getLogger(BookingDetailController.class);

	@Autowired
	private BookingDetailService bookingDetailService;

	@GetMapping(value = "/bookingdetails/find/{id}")
	public ResponseEntity<ResponseMessage> getAllDetailForBooking(@PathVariable("id") int id) {
		List<BookingDetailDto> result = null;
		ResponseMessage rm = new ResponseMessage();
		try {
			result = bookingDetailService.getAllDetailForBooking(id);
			rm.setCode("OK");
			rm.setData(result);
		} catch (Exception ex) {
			rm.setCode("KO");
			List<String> errors = new ArrayList<String>();
			errors.add(ex.toString());
			rm.setErrorMessages(errors);
			logger.error(ex);
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	@GetMapping(value = "/bookingdetails/find/{id}/{idd}")
	public ResponseEntity<ResponseMessage> getDetailForBooking(@PathVariable("id") int id,
			@PathVariable("idd") int idd) {
		BookingDetailDto result = null;
		ResponseMessage rm = new ResponseMessage();
		try {
			result = bookingDetailService.getDetailForBooking(id, idd);
			rm.setCode("OK");
			rm.setData(result);
		} catch (Exception ex) {
			rm.setCode("KO");
			List<String> errors = new ArrayList<String>();
			errors.add(ex.toString());
			rm.setErrorMessages(errors);
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	/*@GetMapping(value = "/bookingdetails/findcode/{code}")
	public ResponseEntity<ResponseMessage> getAllDetailForBookingByCode(@PathVariable("code") String code) {
		List<BookingDetailDto> result = null;
		ResponseMessage rm = new ResponseMessage();
		try {
			result = bookingDetailService.getAllDetailForBookingByCode(code);
			rm.setCode("OK");
			rm.setData(result);
		} catch (Exception ex) {
			rm.setCode("KO");
			List<String> errors = new ArrayList<String>();
			errors.add(ex.toString());
			rm.setErrorMessages(errors);
		}

		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}*/

	@PutMapping(value = "/bookingdetails/update")
	public ResponseEntity<ResponseMessage> updateBookingDetail(@RequestBody BookingDetailDto bookingDetailDto) {
		BookingDetailDto result = null;
		ResponseMessage rm = new ResponseMessage();
		try {
			result = bookingDetailService.update(bookingDetailDto);
			if(result==null) {
				rm.setMessage("Mezzi non disponibili per la data indicata.");
			}
			rm.setCode("OK");
			rm.setData(result);
		} catch (Exception ex) {
			rm.setCode("KO");
			List<String> errors = new ArrayList<String>();
			errors.add(ex.toString());
			rm.setErrorMessages(errors);
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	@PostMapping(value = "/bookingdetails/insert")
	public ResponseEntity<ResponseMessage> insertBookingDetail(@RequestBody BookingDetailDto bookingDetailDto) {
		BookingDetailDto result = null;
		ResponseMessage rm = new ResponseMessage();
		try {
			result = bookingDetailService.insert(bookingDetailDto);
			if(result==null) {
				rm.setMessage("Mezzi non disponibili per la data indicata.");
			}
			rm.setCode("OK");
			rm.setData(result);
		} catch (Exception ex) {
			rm.setCode("KO");
			List<String> errors = new ArrayList<String>();
			errors.add(ex.toString());
			rm.setErrorMessages(errors);
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	@DeleteMapping(value = "/bookingdetails/delete/{id}")
	public ResponseEntity<ResponseMessage> deleteBookingDetail(@PathVariable int id) {
		BookingDetailDto result = null;
		ResponseMessage rm = new ResponseMessage();
		try {
			bookingDetailService.delete(id);
			rm.setCode("OK");
			rm.setData(result);
		} catch (Exception ex) {
			rm.setCode("KO");
			List<String> errors = new ArrayList<String>();
			errors.add(ex.toString());
			rm.setErrorMessages(errors);
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
}