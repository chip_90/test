package com.lipari.highfly.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lipari.highfly.dto.BookingDto;
import com.lipari.highfly.services.RemoteService;
import com.lipari.highfly.services.ServicesService;
import com.lipari.highfly.utils.ResponseMessage;

@RestController
@RequestMapping("/remote")
public class RemoteController {
	private static final Logger logger = Logger.getLogger(RemoteController.class);

	@Autowired
	private RemoteService remoteService;
	
	@Autowired
	private ServicesService servicesService;
	
	@GetMapping("/listallservices")
	public ResponseEntity<ResponseMessage> listAllServices() {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Recupero Lista servizi remota");
		try {
			
			rm.setData(servicesService.getallServices());
			logger.info("Lista Servizi remota Recuperata.");
			rm.setCode("OK");

		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento lista non riuscito");
			logger.error("errore nel recupero della lista servizi remota", e);
			
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/**
	 * Servizio Rest per la creazione di un nuovo booking remoto
	 * 
	 */
	@RequestMapping(value = "/newBooking", method = RequestMethod.POST)
	public ResponseEntity<ResponseMessage> createBooking(@RequestBody BookingDto booking) {		
		ResponseMessage rm = new ResponseMessage();
		try {
			String[] response = remoteService.newBookingRemote(booking);
			rm.setData(response[0]);
			if(!response[1].equals("")) {
				rm.setMessage("Mezzi non disponibili per i seguenti dettagli:"+response[1]);
			}
			rm.setCode("OK");
		} catch (Exception e) {
			rm.setCode("ERRORE: inserimento non riuscito");
			logger.error("ERRORE: inserimento nuovo booking remoto non riuscito", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
}