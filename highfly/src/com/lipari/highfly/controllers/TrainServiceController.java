package com.lipari.highfly.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lipari.highfly.dto.TrainServiceDto;
import com.lipari.highfly.services.TrainServiceService;
import com.lipari.highfly.utils.ResponseMessage;

@RestController
@RequestMapping("/api")
public class TrainServiceController {
	
	private static final Logger logger = Logger.getLogger(TrainServiceController.class);

	@Autowired
	private TrainServiceService trainService;
	
	/**
	 *  -------------------Lista Completa Treno ------------------------------
	 * @return
	 */
	@RequestMapping(value = "/trainsList", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> allTrainsList() {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Recupero Lista Treno...");
		try {
			rm.setData(trainService.listaTrainService());
			logger.info("Lista Treni recuperata.");
			rm.setCode("OK");

		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento lista non riuscito");
			logger.error("errore nel recupero della lista treni", e);
			
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/**
	 *  -------------------Ricerca Treno per Id ------------------------------
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/train/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> getTrain(@PathVariable("id") int id) {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Ricerca Treno...");
		try {
			//Controlliamo che il treno esista....
			if(trainService.findById(id)!=null) {
				rm.setData(trainService.findById(id));
				logger.info("Treno recuperato.");
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: treno non presente");
				logger.error("ERRORE: treno non presente");
			}
		}catch (Exception e) {
			rm.setCode("ERRORE: caricamento treno non riuscito");
			logger.error("errore nel recupero del treno", e);

			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
			
	/***
	 *  -------------------Eliminazione Auto ----------------------------
	 * @param id
	 * @return
	 */

	@RequestMapping(value = "/trainDelete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ResponseMessage> deleteTrainService(@PathVariable("id") int id) {

		logger.info("Ricerca treno da eliminare...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che il treno esista....
			if(trainService.findById(id)!=null) {
				trainService.eliminaTrainService(id);
				rm.setCode("OK");
				logger.info("Treno eliminato");
			}
			else {
				rm.setCode("ERRORE: treno non presente");
				logger.error("ERRORE: treno non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: eliminazione non riuscita");
			logger.error("ERRORE:eliminazione non riuscita", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
		
	/**
	 *  -------------------Aggiorna Treno ----------------------------
	 * @param train
	 * @return
	 */
	@RequestMapping(value = "/trainpdate", method = RequestMethod.PUT)
	public ResponseEntity<ResponseMessage> updateTrainService(@RequestBody TrainServiceDto train) {

		logger.info("Updating Train...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che il treno esista....
			if(trainService.findById(train.getServiceId())!=null) {
				rm.setData(trainService.aggiornaTrainService(train));
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: treno non presente");
				logger.error("ERRORE: treno non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: aggiornamento non riuscito");
			logger.error("ERRORE: aggiornamento non riuscito", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
}