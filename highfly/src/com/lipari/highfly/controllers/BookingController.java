package com.lipari.highfly.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lipari.highfly.dto.BookingDto;
import com.lipari.highfly.services.BookingService;
import com.lipari.highfly.utils.ResponseMessage;

@RestController
@RequestMapping("/api")
public class BookingController {
	
	private static final Logger logger = Logger.getLogger(BookingController.class);
	
	@Autowired
	private BookingService bookingService;
	
	/**
	 * Servizio Rest per richiedere l'intera lista di booking
	 * @return ResponseEntity<ResponseMessage>
	 */
	@RequestMapping(value = "/bookingsList", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> allBookingsList() {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Recupero Lista Booking");
		
		try {
			rm.setData(bookingService.findAllBooking());
			logger.info("Lista Booking Caricata");
			rm.setCode("OK");
		}catch (Exception e)  {
			rm.setCode("Errore: Caricamento lista non riuscito");
			logger.error("Errore: Caricamento lista non riuscito");
			
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/**
	 * Servizio Rest per la creazione di un nuovo booking
	 * @return ResponseEntity<ResponseMessage>
	 */
	@RequestMapping(value = "/booking", method = RequestMethod.POST)
	public ResponseEntity<ResponseMessage> createBooking(@RequestBody BookingDto booking) {		
		ResponseMessage rm = new ResponseMessage();
		try {
			rm.setData(bookingService.saveBooking(booking));
			rm.setCode("OK");
		} catch (Exception e) {
			rm.setCode("ERRORE: inserimento non riuscito");
			logger.error("ERRORE: inserimento nuovo booking non riuscito", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/**
	 * Servizio Rest per la modifica di un booking
	 * @return ResponseEntity<ResponseMessage>
	 */
	@RequestMapping(value = "/bookingUpdate", method = RequestMethod.PUT)
	public ResponseEntity<ResponseMessage> modifyBooking(@RequestBody BookingDto booking) {		
		ResponseMessage rm = new ResponseMessage();
		try {
			rm.setData(bookingService.updateBooking(booking));
			rm.setCode("OK");
		} catch (Exception e) {
			rm.setCode("ERRORE: inserimento non riuscito");
			logger.error("ERRORE: inserimento nuovo booking non riuscito", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/**
	 * Servizio Rest per la ricerca di un booking tramite l'id
	 * @return ResponseEntity<ResponseMessage>
	 */
	@RequestMapping(value = "/booking/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<BookingDto> getBooking(@PathVariable("id") int id) {
		BookingDto booking;
		try {
			booking = bookingService.findBooking(id);
			if (booking == null) {
				System.out.println("booking with id " + id + " not found");
				return new ResponseEntity<BookingDto>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<BookingDto>(booking, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<BookingDto>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * eliminazione del booking con i relativi details
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/bookingDelete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ResponseMessage> deleteBookingWithBookingDetails(@PathVariable("id") int id) {

		logger.info("Ricerca booking da eliminare...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che il booking esista....
			if(bookingService.findBooking(id)!=null) {
				bookingService.deleteBooking(id);
				rm.setCode("OK");
				logger.info("Booking eliminato");
			}
			else {
				rm.setCode("ERRORE: booking non presente");
				logger.error("ERRORE: booking non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: eliminazione non riuscita");
			logger.error("ERRORE:eliminazione non riuscita", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
}