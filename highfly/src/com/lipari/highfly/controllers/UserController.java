package com.lipari.highfly.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lipari.highfly.dto.UserDto;
import com.lipari.highfly.services.UserService;
import com.lipari.highfly.utils.ResponseMessage;

@RestController
@RequestMapping("/api")
public class UserController {
	
	private static final Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	// -------------------Autorizzazione Utenti ------------------------------
	
	@RequestMapping(value = "/userAuthorization", method = RequestMethod.POST)
	public ResponseEntity<ResponseMessage> authorization(@RequestBody UserDto user) {
		
		ResponseMessage rm = new ResponseMessage();
				
		logger.info("Autorizzazione "+user.getUsername()+" in corso...");
		
		try {
			
			rm.setData(userService.autorizzazione(user.getUsername(), user.getPassword()));
			rm.setCode("OK");
			
			if(rm.getData()!=null) {
				logger.info("Utente Autorizzato");
			}
			else {
				logger.info("Utente NON Autorizzato");
				rm.setMessage("Utente NON Autorizzato");
			}
			
		} catch (Exception e) {
			rm.setCode("ERRORE: autorizzazione non riuscita");
			logger.error("Autorizzazione non riuscita");
			
			List<String> eM = new ArrayList<String>();
			eM.add(e.getMessage());
			rm.setErrorMessages(eM);
		}
		
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/**
	 *  -------------------Lista Completa Utenti ------------------------------
	 * @return
	 */
	@RequestMapping(value = "/usersList", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> allUsersList() {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Recupero Lista Utenti...");
		try {
			rm.setData(userService.listaUtenti());
			logger.info("Lista Utenti recuperata.");
			rm.setCode("OK");

		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento lista non riuscito");
			logger.error("errore nel recupero della lista utenti", e);

			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/**
	 *  -------------------Ricerca Utente per Id ------------------------------
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> getUser(@PathVariable("id") int id) {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Ricerca Utente...");
		try {
			//Controlliamo che l'utente esista....
			if(userService.findById(id)!=null) {
				rm.setData(userService.findById(id));
				logger.info("Utente recuperato.");
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: utente non presente");
				logger.error("ERRORE: utente non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento utente non riuscito");
			logger.error("errore nel recupero del'utente", e);

			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	/**
	 *  -------------------Utenti filtrati ------------------------------
	 * @param filter
	 * @return
	 */
	@RequestMapping(value = "/usersFiltered/{filter}", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> usersFiltered(@PathVariable("filter") String filter) {
		
		ResponseMessage rm = new ResponseMessage();
				
		logger.info("Recupero lista utenti fltrati in corso...");
		
		try {
			rm.setData(userService.listaUtentiFiltrati(filter));
			rm.setCode("OK");
			logger.info("Lista utenti fltrata recuperata");
			
		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento utenti filtrati non riuscito");
			rm.getErrorMessages().add(e.getMessage());
			logger.error("ERRORE: caricamento utenti filtrati non riuscito", e);
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	/**
	 *  -------------------Creazione nuovo Utente ----------------------------
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public ResponseEntity<ResponseMessage> createUser(@RequestBody UserDto user) {
		
		logger.info("Creazione dell'utente " + user.getName());
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Se l'utente non esiste gi�....
			if(!userService.findByUsername(user.getUsername())) {
				rm.setData(userService.nuovoUtente(user));
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: utente gi� presente");
				logger.error("ERRORE: utente gi� presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: inserimento non riuscito");
			logger.error("ERRORE: inserimento nuovo utente non riuscito", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
			
	/***
	 *  -------------------Eliminazione Utente ----------------------------
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/userDelete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ResponseMessage> deleteUser(@PathVariable("id") int id) {

		logger.info("Ricerca utente da eliminare...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che l'utente esista....
			if(userService.findById(id)!=null) {
				userService.eliminaUtente(id);
				rm.setCode("OK");
				logger.info("Utente eliminato");
			}
			else {
				rm.setCode("ERRORE: utente non presente");
				logger.error("ERRORE: utente non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: eliminazione non riuscita");
			logger.error("ERRORE:eliminazione non riuscita", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
		
	/**
	 *  -------------------Aggiorna Utente ----------------------------
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/userUpdate", method = RequestMethod.PUT)
	public ResponseEntity<ResponseMessage> updateUser(@RequestBody UserDto user) {

		logger.info("Updating User...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che l'utente esista....
			if(userService.findById(user.getId())!=null) {
				rm.setData(userService.aggiornaUtente(user));
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: utente non presente");
				logger.error("ERRORE: utente non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: aggiornamento non riuscito");
			logger.error("ERRORE: aggiornamento non riuscito", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
}