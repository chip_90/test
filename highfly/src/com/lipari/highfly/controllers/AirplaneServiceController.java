package com.lipari.highfly.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lipari.highfly.dto.AirplaneServiceDto;
import com.lipari.highfly.services.AirplaneServiceService;
import com.lipari.highfly.utils.ResponseMessage;

@RestController
@RequestMapping("/api")
public class AirplaneServiceController {
	
	private static final Logger logger = Logger.getLogger(AirplaneServiceController.class);

	@Autowired
	private AirplaneServiceService airplaneService;
	
	/**
	 *  -------------------Lista Completa Aereo ------------------------------
	 * @return
	 */

	@RequestMapping(value = "/airplanesList", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> allAirplanesList() {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Recupero Lista Aerei...");
		try {
			rm.setData(airplaneService.listaAirplaneService());
			logger.info("Lista Aerei recuperata.");
			rm.setCode("OK");

		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento lista non riuscito");
			logger.error("errore nel recupero della lista aerei", e);
			
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/**
	 *  -------------------Ricerca Aereo per Id ------------------------------
	 * @param id
	 * @return
	 */

	@RequestMapping(value = "/airplane/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> getUser(@PathVariable("id") int id) {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Ricerca Aerei...");
		try {
			//Controlliamo che l'aereo esista....
			if(airplaneService.findById(id)!=null) {
				rm.setData(airplaneService.findById(id));
				logger.info("Aereo recuperata.");
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: aereo non presente");
				logger.error("ERRORE: aereo non presente");
			}


		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento aereo non riuscito");
			logger.error("errore nel recupero dell'aereo", e);

			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/***
	 *  -------------------Eliminazione Aereo ----------------------------
	 * @param id
	 * @return
	 */

	@RequestMapping(value = "/aereoDelete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ResponseMessage> deleteAirplaneService(@PathVariable("id") int id) {

		logger.info("Ricerca aereo da eliminare...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che l'aereo esista....
			if(airplaneService.findById(id)!=null) {
				airplaneService.eliminaAirplaneService(id);
				rm.setCode("OK");
				logger.info("Aereo eliminato");
			}
			else {
				rm.setCode("ERRORE: aereo non presente");
				logger.error("ERRORE: aereo non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: eliminazione non riuscita");
			logger.error("ERRORE:eliminazione non riuscita", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
		
	/**
	 *  -------------------Aggiorna Aereo ----------------------------
	 * @param airplane
	 * @return
	 */

	@RequestMapping(value = "/airplaneUpdate", method = RequestMethod.PUT)
	public ResponseEntity<ResponseMessage> updateAirplaneService(@RequestBody AirplaneServiceDto airplane) {

		logger.info("Updating Airplane...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che l'aereo esista....
			if(airplaneService.findById(airplane.getServiceId())!=null) {
				rm.setData(airplaneService.aggiornaAirplaneService(airplane));
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: aereo non presente");
				logger.error("ERRORE: aereo non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: aggiornamento non riuscito");
			logger.error("ERRORE: aggiornamento non riuscito", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
}