package com.lipari.highfly.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lipari.highfly.dto.ServiceDto;
import com.lipari.highfly.services.ServicesService;
import com.lipari.highfly.utils.ResponseMessage;

@RestController
@RequestMapping("/api")

public class ServicesController {

	@Autowired
	private ServicesService servicesService;

	private static final Logger logger = Logger.getLogger(BookingDetailController.class);

	@GetMapping(value = "/service/{id}")
	public ResponseEntity<ResponseMessage> getServiceById(@PathVariable("id") int id) {

		ServiceDto sDto = null;
		ResponseMessage rm = new ResponseMessage();
		try {
			sDto = servicesService.getServiceById(id);
			if (sDto == null) {
				logger.error("customer non trovato");
				return new ResponseEntity<ResponseMessage>(HttpStatus.NOT_FOUND);
			}
			else {
				rm.setCode("OK");
				rm.setData(sDto);				
			}
		} catch (Exception ex) {
			rm.setCode("KO");
			List<String> errors = new ArrayList<String>();
			errors.add(ex.toString());
			rm.setErrorMessages(errors);
			logger.error(ex);
		}
		
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

}
