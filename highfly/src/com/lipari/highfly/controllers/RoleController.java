package com.lipari.highfly.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lipari.highfly.dto.RoleDto;
import com.lipari.highfly.services.RoleService;
import com.lipari.highfly.utils.ResponseMessage;

@RestController
@RequestMapping("/api")
public class RoleController {
	
	private static final Logger logger = Logger.getLogger(RoleController.class);

	@Autowired
	private RoleService roleService;
	
	/**
	 *  -------------------Lista Completa Ruoli ------------------------------
	 * @return
	 */
	@RequestMapping(value = "/rolesList", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> allRolesList() {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Recupero Lista Ruoli...");
		try {
			rm.setData(roleService.listaRuoli());
			logger.info("Lista Ruoli recuperata.");
			rm.setCode("OK");

		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento lista non riuscito");
			logger.error("errore nel recupero della lista ruoli", e);
			
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	/**
	 *  -------------------Ricerca Ruolo per Id ------------------------------
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/role/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> getRole(@PathVariable("id") int id) {
		
		ResponseMessage rm = new ResponseMessage();
		
		logger.info("Ricerca Ruolo...");
		try {
			//Controlliamo che il ruolo esista....
			if(roleService.findById(id)!=null) {
				rm.setData(roleService.findById(id));
				logger.info("Ruolo recuperato.");
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: ruolo non presente");
				logger.error("ERRORE: ruolo non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: caricamento ruolo non riuscito");
			logger.error("errore nel recupero del ruolo", e);

			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	/**
	 *  -------------------Ruoli filtrati ------------------------------
	 * @param filter
	 * @return
	 */
	@RequestMapping(value = "/rolesFiltered/{filter}", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> usersFiltered(@PathVariable("filter") String filter) {
		
		ResponseMessage rm = new ResponseMessage();
				
		logger.info("Recupero lista ruoli fltrati in corso...");
		
		try {
			rm.setData(roleService.listaRuoliFiltrati(filter));
			rm.setCode("OK");
			logger.info("Lista ruoli fltrata recuperata");
			
		}catch (Exception e) {
			rm.setCode("ERRORE: caricamento ruoli filtrati non riuscito");
			rm.getErrorMessages().add(e.getMessage());
			logger.error("ERRORE: caricamento ruoli filtrati non riuscito", e);
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	/**
	 *  -------------------Creazione nuovo Ruolo ----------------------------
	 * @param role
	 * @return
	 */
	@RequestMapping(value = "/role", method = RequestMethod.POST)
	public ResponseEntity<ResponseMessage> createUser(@RequestBody RoleDto role) {
		
		logger.info("Creazione del ruolo " + role.getDescription());
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Se il ruolo non esiste gi�....
			if(!roleService.findByCode(role.getCode())) {
				rm.setData(roleService.nuovoRuolo(role));
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: ruolo gi� presente");
				logger.error("ERRORE: ruolo gi� presente");
			}
		}catch (Exception e) {
			rm.setCode("ERRORE: inserimento non riuscito");
			logger.error("ERRORE: inserimento nuovo ruolo non riuscito", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
			
	/**
	 *  -------------------Eliminazione Ruoli ----------------------------
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/roleDelete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ResponseMessage> deleteUser(@PathVariable("id") int id) {

		logger.info("Ricerca ruolo da eliminare...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che il ruolo esista....
			if(roleService.findById(id)!=null) {
				roleService.eliminaRuolo(id);
				rm.setCode("OK");
				logger.info("Ruolo eliminato");
			}
			else {
				rm.setCode("ERRORE: ruolo non presente");
				logger.error("ERRORE: ruolo non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: eliminazione non riuscita");
			logger.error("ERRORE:eliminazione non riuscita", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
		
	/**
	 *  -------------------Aggiorna Ruolo ----------------------------
	 * @param role
	 * @return
	 */
	@RequestMapping(value = "/roleUpdate", method = RequestMethod.PUT)
	public ResponseEntity<ResponseMessage> updateUser(@RequestBody RoleDto role) {

		logger.info("Updating Role...");
		
		ResponseMessage rm = new ResponseMessage();
		try {
			//Controlliamo che il ruolo esista....
			if(roleService.findById(role.getId())!=null) {
				rm.setData(roleService.aggiornaRuolo(role));
				rm.setCode("OK");
			}
			else {
				rm.setCode("ERRORE: ruolo non presente");
				logger.error("ERRORE: ruolo non presente");
			}
		} catch (Exception e) {
			rm.setCode("ERRORE: aggiornamento non riuscito");
			logger.error("ERRORE: aggiornamento non riuscito", e);
			rm.getErrorMessages().add(e.getMessage());
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
}