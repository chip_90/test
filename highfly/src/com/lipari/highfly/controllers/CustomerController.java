package com.lipari.highfly.controllers;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.lipari.highfly.dto.CustomerDto;
import com.lipari.highfly.services.CustomerService;
import com.lipari.highfly.utils.ResponseMessage;

@RestController
@RequestMapping("/api")
public class CustomerController {

	private static final Logger logger = Logger.getLogger(CustomerController.class);

	@Autowired
	@Qualifier("customerService")
	private CustomerService customerService;

	/**
	 * rest per prelevare un customer
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/customer/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> getCustomer(@PathVariable("id") int id) {

		CustomerDto customer;
		ResponseMessage responseMessage = new ResponseMessage();

		try {
			customer = customerService.findCustomertById(id);
			responseMessage.setCode("OK");
			responseMessage.setData(customer);
			if (customer == null) {
				logger.error("customer non trovato");
				return new ResponseEntity<ResponseMessage>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("problema interno", e);
			return new ResponseEntity<ResponseMessage>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 *rest per l' eliminazione logica di un customer
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/customer/delete/{id}", method = RequestMethod.PUT)
	public ResponseEntity<ResponseMessage> softDeleteCustomer(@PathVariable("id") int id) {
		ResponseMessage responseMessage = new ResponseMessage();

		try {
			customerService.softDelete(id);
			responseMessage.setCode("OK");
		} catch (Exception e) {
			logger.error("Errore durante l'eliminazione del Customer", e);
			return new ResponseEntity<ResponseMessage>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.OK);
	}

	/**
	 *rest per la crezione di un nuovo customer
	 * @param customerDto
	 * @param ucBuilder
	 * @return
	 */
	@RequestMapping(value = "/customer/", method = RequestMethod.POST)
	public ResponseEntity<Void> createCustomer(@RequestBody CustomerDto customerDto, UriComponentsBuilder ucBuilder) {

		customerService.saveCustomer(customerDto);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/customer/{id}").buildAndExpand(customerDto.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	/**
	 * rest per l' aggiornamento di un customer
	 * @param id
	 * @param customer
	 * @return
	 */
	@RequestMapping(value = "/customer/{id}", method = RequestMethod.PUT)
	public ResponseEntity<CustomerDto> updateCustomer(@PathVariable("id") int id, @RequestBody CustomerDto customer) {

		CustomerDto currentCustomerDto = new CustomerDto();

		try {
			currentCustomerDto = customerService.findCustomertById(id);
			if (currentCustomerDto == null) {
				System.out.println("Customer with id " + id + " not found");
				return new ResponseEntity<CustomerDto>(HttpStatus.NOT_FOUND);
			}

			customerService.updateCustomer(customer);

			return new ResponseEntity<CustomerDto>(customer, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("update fallito", e);
			return new ResponseEntity<CustomerDto>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * rest che restituisce la lista dei customers
	 * @return
	 */
	@RequestMapping(value = "/customer/AllCustomers", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> listAllCustomers() {
		List<CustomerDto> customerDtos;
		ResponseMessage responseMessage = new ResponseMessage();
		try {

			customerDtos = customerService.findAllCustomers();
			responseMessage.setCode("OK");
			responseMessage.setData(customerDtos);

			if (customerDtos.isEmpty()) {
				return new ResponseEntity<ResponseMessage>(HttpStatus.NO_CONTENT);
			}

		} catch (Exception e) {
			logger.error("Errore durante il caricamento della lista", e);
			responseMessage.setCode("KO");
			responseMessage.getErrorMessages().add("ERrore generico");
		}
		return new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.OK);
	}
}