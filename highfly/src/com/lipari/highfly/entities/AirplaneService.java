package com.lipari.highfly.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * The persistent class for the airplane_service_joined database table.
 * 
 */
@Entity
@Table(name="airplane_service")
@DiscriminatorValue(value="Airplane")
public class AirplaneService extends Service implements Serializable {

	private static final long serialVersionUID = -4570586812887261365L;

	@Column(name="seats_first_class")
	private Integer seatsFirstClass;

	@Column(name="seats_second_class")
	private Integer seatsSecondClass;

	public AirplaneService() {
	}

	public Integer getSeatsFirstClass() {
		return seatsFirstClass;
	}

	public void setSeatsFirstClass(Integer seatsFirstClass) {
		this.seatsFirstClass = seatsFirstClass;
	}

	public Integer getSeatsSecondClass() {
		return seatsSecondClass;
	}

	public void setSeatsSecondClass(Integer seatsSecondClass) {
		this.seatsSecondClass = seatsSecondClass;
	}

}