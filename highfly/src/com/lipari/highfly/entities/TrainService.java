package com.lipari.highfly.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * The persistent class for the train_service_joined database table.
 * 
 */
@Entity
@Table(name="train_service")
@DiscriminatorValue(value="Train")
public class TrainService extends Service implements Serializable {

	private static final long serialVersionUID = -8774787940422789764L;

	@Column(name="type")
	private String type;

	@Column(name="wagons_number")
	private Integer wagonsNumber;

	public TrainService() {
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getWagonsNumber() {
		return wagonsNumber;
	}

	public void setWagonsNumber(Integer wagonsNumber) {
		this.wagonsNumber = wagonsNumber;
	}

}