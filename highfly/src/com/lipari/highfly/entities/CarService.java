package com.lipari.highfly.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * The persistent class for the car_service_joined database table.
 * 
 */
@Entity
@Table(name="car_service")
@DiscriminatorValue(value="Car")
public class CarService extends Service implements Serializable {

	private static final long serialVersionUID = -2920802406997258565L;

	@Column(name="manufacturer")
	private String manufacturer;

	@Column(name="supply")
	private String supply;

	@Column(name="vehicle_quantity")
	private int vehicleQuantity;

	public CarService() {
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getSupply() {
		return this.supply;
	}

	public void setSupply(String supply) {
		this.supply = supply;
	}

	public int getVehicleQuantity() {
		return this.vehicleQuantity;
	}

	public void setVehicleQuantity(int vehicleQuantity) {
		this.vehicleQuantity = vehicleQuantity;
	}

}