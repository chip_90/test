package com.lipari.highfly.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the booking database table.
 * 
 */
@Entity
@Table(name="booking")
public class Booking implements Serializable {

	private static final long serialVersionUID = 6689024584435934889L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;

	@Column(name="code")
	private String code;

	@Column(name="date")
	private Timestamp date;

	@Column(name="nominative")
	private String nominative;
	
	@Column(name="n_partecipants")
	private int nPartecipants;

	@Column(name="state")
	private String state;

	@Version
	private int version;

	//bi-directional many-to-one association to Customer
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="customer_id", referencedColumnName ="id")
	private Customer customer;

	//bi-directional many-to-one association to BookingDetail ,
	@OneToMany(fetch=FetchType.LAZY, mappedBy="booking", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
	private List<BookingDetail> bookingDetails;

	public Booking() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getNominative() {
		return this.nominative;
	}

	public void setNominative(String nominative) {
		this.nominative = nominative;
	}
	
	public int getnPartecipants() {
		return nPartecipants;
	}

	public void setnPartecipants(int nPartecipants) {
		this.nPartecipants = nPartecipants;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<BookingDetail> getBookingDetails() {
		return this.bookingDetails;
	}

	public void setBookingDetails(List<BookingDetail> bookingDetails) {
		this.bookingDetails = bookingDetails;
	}

}